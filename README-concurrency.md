Aquinas' most complicated backend activity occurs on the Git host. A
queueing service, queued, runs there to keep jobs from conflicting
with each other. Originally, queued serialized all jobs, but that
became a problem when some projects began to require an extended period
of time to grade. Thus queued now executes jobs in Go routines, while
limiting how many Go routines can execute at once. This describes the
locking that governs this concurrency.

To prevent deadlock, locks are requested in the same order by all Go
routines. The ordering of the locks is alphabetical, based on the symbol
names associated with each lock.

First, the following jobs lock the whole system (lockerAllWrite), and
thus prevent any other jobs from proceeding.

  * aquinas-initialize-projects
  * aquinas-add-student-slave
  * aquinas-remove-student-slave
  * aquinas-fork-project

The grading job locks other grading tasks (lockerGrading). This could
become more fine-grained in the future, but that will require a more
careful analysis to determine what aspects of the system present critical
sections while grading. Such work will become worthwhile should the
performance of grading become more critical.

  * grader

The remaining tasks are more finely locked. This allows for checking
whether a project is forked, checking whether grading is taking place,
and so on even while grading is underway. I note the shared resources
used by each job.

  * aquinas-is-forked
    * Read user information from account database (lockerAllRead)
    * Check the existence of ~/PROJECT (no lock)

  * aquinas-is-grading
    * Check the existence of /tmp/STUDENT/grading/PROJECT (no lock)

  * aquinas-get-ssh-authorized-keys
    * Read user information from account database (lockerAllRead)
    * Read ~/.ssh/authorized_keys (lockerSSH)

  * aquinas-deploy-key
    * Read user information from account database (lockerAllRead)
    * Possibly create ~/.ssh (lockerSSH)
    * Write ~/.ssh/authorized_keys (lockerSSH)

Note that lockerSSH locks accesses to all user's SSH parameters. This
could be made to be a per-user lock in the future.
