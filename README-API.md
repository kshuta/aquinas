*Note that this API is not yet settled.*

Aquinas provides an HTTP-based API that allows clients to interact with
its features. The default client is comprised of the HTML, JavaScript, and
WebAssembly provided by Aquinas. However, it is possible to build other
clients through the use of the Aquinas API, which this document describes.

Requires HTTP basic authentication:

GET /api/students/<email>

	StatusOK: Returns JSON-encoded data describing the named
	student. Students can retrieve only their own data. Otherwise:
	returns a plaintext error message.

POST /api/students/<email>

	description: JSON-encoded data describing the named student.

	StatusOK: Updates the student fields present in description
	and returns true. Leaves fields not present in description
	unchanged. Students can set only their own data. Otherwise:
	returns a plaintext error message.

DELETE /api/students/<email>

	StatusOK: Permantently deletes the named student and all
	resources associated with that student. Students can delete only
	themselves. Otherwise: returns a plaintext error message.

GET /api/courses

	StatusOK: Returns a JSON-encoded list of the courses in which
	the signed-in student is enrolled. Otherwise: returns a plaintext
	error message.

GET /api/stats-ranks

	StatusOK: Returns JSON-encoded mapping between ranks and lists
	of students, where rank is number of projects complete with a
	passing outcome. Otherwise: returns a plaintext error message.

GET /api/stats-courses/<course>

	StatusOK: Returns JSON-encoded grades awarded in the given course.
	Other student names are redacted unless a teacher performs
	the request. Otherwise: returns a plaintext error message.

GET /api/stats-grades

	student: an optional string that selects the student to query
	project: an optional string that selects the project to query
	brief:   a Boolean

	StatusOK: Returns JSON-encoded grades. Students can retrieve
	their own grades; teachers can retrieve all grades. Otherwise:
	returns a plaintext error message.

GET /api/grades-now

	project: a string that selects the project to grade and query

	StatusOK: Forces Aquinas to re-grade the named project, and
	returns JSON-encoded grades. Otherwise: returns a plaintext
	error message.

GET /api/is-grading

	project: a string that selects the project to query

	StatusOK: Determines whether Aquinas is presently grading the
	named project. Returns a UUID. UUID contains all zeros if
	Aquinas not presently grading the named project, otherwise
	subsequent call to /api/wait will wait for job corresponding
	with UUID to complete.

Does not require authentication:

GET /api/projects

	tag: an optional tag that will restrict the result, e.g.,
	a named challenge series or academic course

	StatusOK: Returns JSON-encoded list of projects, excluding
	sensitive information such as checks. Otherwise: returns a
	plaintext error message.

GET /api/project/<projname>

	While this call does not strictly require authentication,
	the result will differ depending on whether the caller is
	authenticated. If authenticated, the result will contain
	student-specific information about how to complete the project,
	results, and so on.

	StatusOK: Returns a JSON-encoded representation of the
	named project (e.g., "git" or "helloC"), excluding sensitive
	information such as checks and including an HTML description of
	the project. Otherwise: returns a plaintext error message.

POST /api/project/<projname>/fork

	StatusOK: Forks the given project, providing a personal repository
	for the student. Returns a UUID; a call to /api/wait with this
	UUID will wait for the fork to complete. Otherwise: returns a
	plaintext error message.

GET /api/wait

	id, a UUID that identifies a job

	Calling wait will block until the job identified by uuid is
	complete.

	StatusOK: Returns true. Otherwise: returns a plaintext error
	message.

GET /api/notify

	Notify is a call used internally by Aquinas components to alert
	Aquinas' HTTP daemon that a job has completed. The HTTP daemon
	will, in turn, alert any clients that have waited on the same job.

GET /api/register/<email>

	Register a new student. Success will result in an email sent to
	the student that prompts him to complete the registration. This
	email will contain values necessary for completing the process
	with POST /api/register.

	StatusOK: Returns true. Otherwise: returns a plaintext error
	message.

POST /api/register/<email>

	nonce:    a nonce, provided by Aquinas.
	token:    the hash of a secret value, provided by Aquinas.
	password: a string representing the student's password.

	Register a student. Aquinas must have provided the nonce and hash
	in an email in response to the preceeding step (/api/register).

	StatusOK: Returns a UUID; a call to /api/wait with this UUID
	will wait for the account creation to complete. Otherwise:
	returns a plaintext error message.

GET /api/reset-password/<email>

	Begin to reset a student's password. Success will result in
	an email sent to the student that prompts him to complete the
	password reset. This email will contain values necessary for
	completing the process with POST /api/reset-password.

	StatusOK: Returns true. Otherwise: returns a plaintext error
	message.

POST /api/reset-password/<email>

	nonce:    a nonce, provided by Aquinas.
	token:    the hash of a secret value, provided by Aquinas.
	password: a string representing the new password.

	Reset a student's password. Aquinas must have provided the
	nonce and hash in an email in response to the preceeding step
	(/api/reset-password).

	StatusOK: Returns true. Otherwise: returns a plaintext error
	message.
