package main

import (
	"aquinas/common"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "usage: %s USER\n", os.Args[0])
		os.Exit(1)
	}

	student := os.Args[1]
	norm := common.NormalizeUsername(student)

	u, err := user.Lookup(norm)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not lookup user %s: %s\n", norm, err)
		os.Exit(1)
	}

	keyPath := path.Join(u.HomeDir, ".ssh", "authorized_keys")
	key, err := ioutil.ReadFile(keyPath)
	if err != nil {
		if !os.IsNotExist(err) {
			fmt.Fprintf(os.Stderr, "could not read key for %s: %s\n", norm, err)
			os.Exit(1)
		}

		// In case of not found, ignore and provide empty key.
		key = []byte("\n")
	}

	fmt.Printf("%s", string(key))
}
