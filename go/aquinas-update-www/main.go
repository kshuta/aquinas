package main

import (
	"aquinas/conf"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"aquinas/proj"
	"aquinas/run"
	"strings"
)

func main() {
	var g proj.Grade
	var grades []proj.Grade
	var d, t string

	if len(os.Args) != 4 {
		fmt.Fprintf(os.Stderr, "usage: %s STUDENT PROJECT LANGUAGE\n", os.Args[0])
		os.Exit(1)
	}

	student := os.Args[1]
	project := os.Args[2]
	language := os.Args[3]

	recordPath := path.Join(conf.DataRoot(), "teacher", "workdir", "records",
	                        project, student + language + ".record")

	f, err := os.Open(recordPath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not open: %s\n", err)
		os.Exit(1)
	}

	defer f.Close()

	for {
		n, err := fmt.Fscanln(f, &d, &t, &g.Student, &g.Project, &g.Commit, &g.Outcome)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Fprintf(os.Stderr, "could not parse %s: %s\n", recordPath, err)
			os.Exit(1)
		}
		if n != 6 {
			fmt.Fprintf(os.Stderr, "could not parse %s: not enough fields\n", recordPath)
			os.Exit(1)
		}

		g.Timestamp = d + " " + t

		grades = append(grades, g)
	}

	j, err := json.Marshal(grades)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not encode %v: %s\n", grades, err)
		os.Exit(1)
	}

	lastPath  := path.Join(conf.DataRoot(), "teacher", "workdir", "records",
                              project, student + language + ".last")
	last, err := ioutil.ReadFile(lastPath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not read %s: %s\n", lastPath, err)
		os.Exit(1)
	}

	stdin := io.MultiReader(
		strings.NewReader("record\n"),
		strings.NewReader(string(j) + "\n"),
		strings.NewReader(string(last) + "\n"),
	)

        if _, _, err := run.SSH(stdin, nil, "teacher@" + conf.HostWWW() + "." + conf.Domain()); err != nil {
		fmt.Fprintf(os.Stderr, "could not SSH run teachersh: %s", err)
		os.Exit(1)
	}

	os.Remove(lastPath)
}
