package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"github.com/google/uuid"
)

func main() {
	var student, projectName string

	if len(os.Args) != 3 {
		fmt.Fprintf(os.Stderr, "usage: %s USER PROJECT\n", os.Args[0])
		os.Exit(1)
	}

	student = os.Args[1]
	projectName = os.Args[2]

	file := path.Join("/tmp", student, "grading", projectName)
	jobUUID, err := ioutil.ReadFile(file)
	if err != nil {
		if !os.IsNotExist(err) {
			fmt.Fprintf(os.Stderr, "could not determine grading status: %s\n", err)
		}

		jobUUID = []byte(uuid.Nil.String())
	}

	fmt.Println(string(jobUUID))
}
