package main

import (
	"aquinas/conf"
	"aquinas/course"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path"
	"strconv"
)

func main() {
	var courses []course.Course

	if len(os.Args) < 3 {
		fmt.Fprintf(os.Stderr, "usage: %s COURSE-YEAR-SEMESTER EVENT [EVENT...]\n", os.Args[0])
		os.Exit(1)
	}

	course := os.Args[1]

	p := path.Join(conf.ConfRoot(), "httpd/courses/courses")
	f, err := os.Open(p)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not open %s: %s\n", p, err)
		os.Exit(1)
	}

	defer f.Close()
	dec := json.NewDecoder(f)

	if err := dec.Decode(&courses); err != nil {
		fmt.Fprintf(os.Stderr, "could not decode %s: %s\n", p, err)
	}

	courseFound := false
	for i, c := range(courses) {
		if c.Name == course {
			courseFound = true

			for _, event := range(os.Args[2:]) {
				eventFound := false
				for _, event2 := range(courses[i].Events) {
					if event == event2.Name {
						eventFound = true
						break
					}
				}

				if !eventFound {
					fmt.Fprintf(os.Stderr, "event %s not found in %s\n", event, course)
					os.Exit(1)
				}

				for _, student := range(courses[i].Enrollment) {
					var grade float64

					fmt.Printf("Enter %s grade for %s: ", event, student)

					if n, err := fmt.Scanf("%f\n", &grade); err != nil {
						fmt.Fprintf(os.Stderr, "could not read grade: %s\n", err)
						os.Exit(1)
					} else if n != 1 {
						fmt.Fprintf(os.Stderr, "could not parse input\n")
						os.Exit(1)
					}

					p := path.Join("/mnt/xvdb/www/", student, fmt.Sprintf("%s-%s-score", event, courses[i].Name))
					if err := ioutil.WriteFile(p, []byte(fmt.Sprintf("%f\n", grade)), 0644); err != nil {
						fmt.Fprintf(os.Stderr, "could not write grade: %s\n", err)
						continue
					}

					p = path.Join("/mnt/xvdb/www/", student, event + "-records")
					if _, err := os.Stat(p); err != nil {
						if os.IsNotExist(err) {
							fmt.Fprintf(os.Stderr, "%s does not exist; will create\n", p)

							if err := ioutil.WriteFile(p, []byte(fmt.Sprintf("[]\n")), 0660); err != nil {
								fmt.Fprintf(os.Stderr, "could not write empty record file: %s\n", err)
								os.Exit(1)
							}

							u, err := user.Lookup("teacher")
							if err != nil {
								fmt.Fprintf(os.Stderr, "could not lookup teacher: %s\n", err)
								os.Exit(1)
							}

							uid, err := strconv.ParseInt(u.Uid, 10, 32)
							if err != nil {
								fmt.Fprintf(os.Stderr, "could not parse UID %s: %s\n", u.Uid, err)
								os.Exit(1)
							}

							g, err := user.LookupGroup("www-data")
							if err != nil {
								fmt.Fprintf(os.Stderr, "could not group www-data: %s\n", err)
								os.Exit(1)
							}

							gid, err := strconv.ParseInt(g.Gid, 10, 32)
							if err != nil {
								fmt.Fprintf(os.Stderr, "could not parse GID %s: %s\n", g.Gid, err)
								os.Exit(1)
							}

							if err := os.Chown(p, int(uid), int(gid)); err != nil {
								fmt.Fprintf(os.Stderr, "could not chown new record file: %s\n", err)
								os.Exit(1)
							}
						}
					}
				}
			}
		}
	}

	if !courseFound {
		fmt.Fprintf(os.Stderr, "course %s not found\n", course)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stderr, "Please restart httpd, so that it reflects these changes.\n")
}
