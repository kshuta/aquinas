package main

/*
 * This program creates and removes users on behalf of the Aquinas WWW
 * server. This program is meant to be installed as a shell on the Aquinas
 * Git server, as it reads its input from stdin. The Aquinas WWW server uses
 * SSH to establish an authenticated and encrypted connection to provide
 * such input.
 */

import (
	"bufio"
	"aquinas/common"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"log/syslog"
	"os"
	"aquinas/queue"
	"strings"
)

func main() {
	var op, arg1, arg2, arg3, arg4 string

	logger, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		panic(err)
	}

	reader := bufio.NewReader(os.Stdin)

	line, err := reader.ReadString('\n')
	if err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not read command: %s", err))
	}

	line = strings.TrimSuffix(line, "\n")

	n, err := fmt.Sscanf(line, "%s %s %s %s %s", &op, &arg1, &arg2, &arg3, &arg4)
	if err != nil && err != io.EOF {
		common.Fail(logger.Err, fmt.Errorf("could not parse command: %s", err))
	}

	logger.Debug("executing " + line)

	switch op {
	case "ssh":
		if n != 2 {
			common.Fail(logger.Err, errors.New("wrong arg count for ssh command"))
		}

		student := arg1

		output, _, err := queue.Enqueue("-s", "/usr/sbin/aquinas-get-ssh-authorized-keys", student)
		if err != nil {
			common.Fail(logger.Err, fmt.Errorf("could not enqueue ssh command: %s", err))
		}

		fmt.Printf("%s", string(output))
	case "add":
		if n != 2 {
			common.Fail(logger.Err, errors.New("wrong arg count for add command"))
		}

		student := arg1

		_, jobUUID, err := queue.Enqueue("-n", "/usr/sbin/aquinas-add-student-slave", student)
		if err != nil {
			common.Fail(logger.Err, fmt.Errorf("could not enqueue add command: %s", err))
		}

		fmt.Printf("%s", string(jobUUID))
	case "remove":
		if n != 2 {
			common.Fail(logger.Err, errors.New("wrong arg count for remove command"))
		}

		student := arg1

		_, jobUUID, err := queue.Enqueue("-n", "/usr/sbin/aquinas-remove-student-slave", student)
		if err != nil {
			common.Fail(logger.Err, fmt.Errorf("could not enqueue remove command: %s", err))
		}

		fmt.Printf("%s", string(jobUUID))
	case "key":
		if n != 3 {
			common.Fail(logger.Err, errors.New("wrong arg count for key command"))
		}

		student := arg1

		key, err := base64.StdEncoding.DecodeString(arg2)
		if err != nil {
			common.Fail(logger.Err, fmt.Errorf("could decode key argument: %s", err))
		}

		_, jobUUID, err := queue.Enqueue("-n", "/usr/sbin/aquinas-deploy-key", student, string(key))
		if err != nil {
			common.Fail(logger.Err, fmt.Errorf("could not enqueue key command: %s", err))
		}

		fmt.Printf("%s", string(jobUUID))
	case "check":
		if n != 4 {
			common.Fail(logger.Err, errors.New("wrong arg count for check command"))
		}

		student := arg1
		proj    := arg2
		gitJSON := arg3

		stdout, stderr, err := queue.Enqueue("-s", "/usr/sbin/grader", student, proj, gitJSON)
		if err != nil {
			common.Fail(logger.Err, fmt.Errorf("could not enqueue check command: %s", err))
		}

		fmt.Fprintf(os.Stdout, "%s", string(stdout))
		fmt.Fprintf(os.Stderr, "%s", string(stderr))
	case "is-forked":
		if n != 4 {
			common.Fail(logger.Err, errors.New("wrong arg count for is-forked command"))
		}

		student := arg1
		proj    := arg2
		gitJSON := arg3

		output, _, err := queue.Enqueue("-s", "/usr/sbin/aquinas-is-forked", student, proj, gitJSON)
		if err != nil {
			common.Fail(logger.Err, fmt.Errorf("could not enqueue ssh command: %s", err))
		}

		fmt.Printf("%s", string(output))
	case "fork":
		if n != 4 && n != 5 {
			common.Fail(logger.Err, errors.New("wrong arg count for fork command"))
		}

		args := []string{
			"-n",
			"/usr/sbin/aquinas-fork-project",
			arg1, // Student.
			arg2, // Project.
			arg3, // gitJSON.
		}

		if n == 5 {
			args = append(args, arg4) // Optional language.
		}

		_, jobUUID, err := queue.Enqueue(args...)
		if err != nil {
			common.Fail(logger.Err, fmt.Errorf("could not enqueue fork command: %s", err))
		}

		fmt.Printf("%s", string(jobUUID))
	case "is-grading":
		if n != 3 {
			common.Fail(logger.Err, errors.New("wrong arg count for is-grading command"))
		}

		args := []string{
			"-s",
			"/usr/sbin/aquinas-is-grading",
			arg1, // Student.
			arg2, // Project
		}

		output, _, err := queue.Enqueue(args...)
		if err != nil {
			common.Fail(logger.Err, fmt.Errorf("could not enqueue is-grading command: %s", err))
		}

		fmt.Printf("%s", string(output))
	default:
		common.Fail(logger.Err, errors.New("illegal input to " + os.Args[0]))
	}
}
