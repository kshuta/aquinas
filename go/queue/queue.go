package queue

import (
	"aquinas/conf"
	"encoding/json"
	"net"
	"aquinas/proj"
	"strings"
)

// Enqueue adds a job.
func Enqueue(args ...string) (stdout, stderr []byte, err error) {
	var result proj.Result

	c, err := net.DialUnix("unix", nil, &net.UnixAddr{conf.SockPath, "unix"})
	if err != nil {
		return
	}
	defer c.Close()

	_, err = c.Write([]byte(strings.Join(args, "\x00") + "\n"))
	if err != nil {
		return
	}

	dec := json.NewDecoder(c)
	if err = dec.Decode(&result); err != nil {
		return
	}

	return result.Stdout, result.Stderr, nil
}
