package main

import (
	"flyn.org/git/wasm.git/sugar"
	"net/url"
	"flyn.org/git/safe.git"
	"aquinas/student"
	"aquinas/wasm"
)

func gitProvider2(file, email, params string) {
	gitProvider := new(safe.String)
	gitPath     := new(safe.String)
	gitUsername := new(safe.String)
	gitToken    := new(safe.String)

	title := "Change Git provider"
	descr := "Change the Git provider associated with your account"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	_, ok := values["git-provider"]
	if !ok {
		sugar.DisplayLocalErr(document, content, "missing git-provider value")
		return
	}

	if err := gitProvider.UnmarshalText([]byte(values.Get("git-provider"))); err != nil {
                sugar.DisplayLocalErr(document, content, "ill-formed Git provider")
                return
        }

	switch gitProvider.String() {
	case "gitlab":
		var ok bool

		_, ok = values["git-path"]
		if !ok {
			sugar.DisplayLocalErr(document, content, "missing git-path value")
			return
		}

		if err := gitPath.UnmarshalText([]byte(values.Get("git-path"))); err != nil {
			sugar.DisplayLocalErr(document, content, "ill-formed Git path")
			return
		}

		_, ok = values["git-username"]
		if !ok {
			sugar.DisplayLocalErr(document, content, "missing git-username value")
			return
		}

		if err := gitUsername.UnmarshalText([]byte(values.Get("git-username"))); err != nil {
			sugar.DisplayLocalErr(document, content, "ill-formed Git username")
			return
		}

		_, ok = values["git-token"]
		if !ok {
			sugar.DisplayLocalErr(document, content, "missing git-token value")
			return
		}

		if err := gitToken.UnmarshalText([]byte(values.Get("git-token"))); err != nil {
			sugar.DisplayLocalErr(document, content, "ill-formed Git token")
			return
		}
	}

	if err := wasm.PostStudent(email, student.Student{
		GitProvider: gitProvider,
		GitPath:     gitPath,
		GitUsername: gitUsername,
		GitToken:    gitToken,
	}); err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
	}

	p    := sugar.CreateElement(document, "p")
	text := `Git provider changed. Please <a href="/">click</a> to continue.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)
}
