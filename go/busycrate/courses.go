package main

import (
	"flyn.org/git/wasm.git/sugar"
	"encoding/json"
	"fmt"
	"aquinas/proj"
	"sort"
	"strings"
	"syscall/js"
	"aquinas/wasm"
)

type points struct {
	score float64
	max   float64
}

func renderGrades(this js.Value, args []js.Value) interface{} {
	// Student -> Project -> Grade.
	students    := []string{}
	projects    := []string{}
	grades      := make(map[string]map[string]proj.Grade)
	pts         := make(map[string]points)
	selfPresent := false

	student := args[0].String()
	data := args[1].String()

	document := sugar.GlobalGet("document")
	table := sugar.GetElementByID(document, "main-content-table")
	table.Set("innerHTML", "")

	var raw map[string][]proj.Grade
	json.Unmarshal([]byte(data), &raw)

	// The structure of the data in raw/data/projects maintains the order of
	// projects. Note that each student's record contains all of the course's
	// projects, so we just iterate the first one.
	for _, v := range raw {
		for _, g := range v {
			projects = append(projects, g.Project)
		}

		break
	}

	for k, v := range raw {
		// Ensure requesting student is first column.
		if k == student {
			selfPresent = true
			students = append([]string{k}, students...)
		} else {
			students = append(students, k)
		}

		// This reorganizes things in grades for use below.
		for _, g := range v {
			if _, ok := grades[k]; !ok {
				grades[k] = make(map[string]proj.Grade)
			}

			grades[k][g.Project] = g
		}
	}

	// Sort students, but leave first (non-redacted self) alone if present.
	if selfPresent {
		sort.Strings(students[1:])
	} else {
		sort.Strings(students)
	}

	tr := sugar.CreateElement(document, "tr")
	sugar.AppendChild(table, tr)

	// See https://www.jimmybonney.com/articles/column_header_rotation_css/.
	for _, s := range append([]string{"Event"}, students...) {
		th := sugar.CreateElement(document, "th")
		sugar.SetAttribute(th, "class", "booktabs-rotate")

		div := sugar.CreateElement(document, "div")
		sugar.AppendChild(th, div)

		span := sugar.CreateElement(document, "span")
		span.Set("innerHTML", s)
		sugar.AppendChild(div, span)

		sugar.AppendChild(tr, th)
	}

	for _, p := range projects {
		tr := sugar.CreateElement(document, "tr")
		sugar.AppendChild(table, tr)

		td := sugar.CreateElement(document, "td")
		sugar.SetAttribute(td, "class", "booktabs")
		td.Set("innerHTML", p)
		sugar.AppendChild(tr, td)

		for _, s := range students {
			val := ""

			g, ok := grades[s][p]
			if ok && g.Score > -1 {
				pt := pts[s]
				pt.score += g.Score
				pt.max   += g.Max
				pts[s] = pt

				if (g.Outcome != "") {
					val = fmt.Sprintf("%s@%s (%.1f/%.1f)",  g.Outcome, g.Timestamp[5:16], g.Score, g.Max)
				} else {
					val = fmt.Sprintf("No autograde (%.1f/%.1f)",  g.Score, g.Max)
				}
			} else if ok && g.Outcome != "" {
				val = fmt.Sprintf("%s@%s",  g.Outcome, g.Timestamp[5:16])
			}

			td := sugar.CreateElement(document, "td")
			sugar.SetAttribute(td, "class", "booktabs")
			td.Set("innerHTML", val)
			sugar.AppendChild(tr, td)
		}
	}

	tr = sugar.CreateElement(document, "tr")
	sugar.SetAttribute(tr, "class", "booktabs-bottom")
	sugar.AppendChild(table, tr)

	td := sugar.CreateElement(document, "td")
	sugar.SetAttribute(td, "class", "booktabs")
	td.Set("innerHTML", "Total")
	sugar.AppendChild(tr, td)

	for _, s := range students {
		val := "0/0"

		t, ok := pts[s]
		if ok {
			val = fmt.Sprintf("%.1f/%.1f (%.1f%%)",  t.score, t.max, t.score / t.max * 100)
		}

		td := sugar.CreateElement(document, "td")
		sugar.SetAttribute(td, "class", "booktabs")
		td.Set("innerHTML", val)
		sugar.AppendChild(tr, td)
	}

	return nil
}


func getGrades(this js.Value, args []js.Value) interface{} {
	student := args[0].String()

	document := sugar.GlobalGet("document")
	input := sugar.GetElementByID(document, "main-content-select")
	course := input.Get("value").String()

	go func() {
		grades, err := wasm.GetStatsCourse(course)
		if err != nil {
			return
		}

		gradesEnc, err := json.Marshal(grades)
		if err != nil {
			return
		}

		js.FuncOf(renderGrades).Invoke(student, string(gradesEnc))
	}()

	return nil
}

func courses(file, student, ignored string) {
	title := "Course grades"
	descr := "List of student grades"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	courseNames, err := wasm.GetCourses()
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	if len(courseNames) == 0 {
		p := sugar.CreateElement(document, "p")
		p.Set("innerHTML", "Not enrolled in a course")
		sugar.AppendChild(content, p)
		return
	}

	grades, err := wasm.GetStatsCourse(courseNames[0])
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	data, err := json.Marshal(grades)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	form := sugar.CreateElement(document, "form")
	sugar.AppendChild(content, form)

	label := sugar.CreateElement(document, "label")
	label.Set("innerHTML", "Course:")
	sugar.AppendChild(form, label)

	sel := sugar.CreateElement(document, "select")
	sel.Set("name", "course")
	sel.Set("id", "main-content-select")
	sugar.AddEventListener(sel, "change", js.FuncOf(
		func(this js.Value, args []js.Value) interface{} {
			js.FuncOf(getGrades).Invoke(student)
			return nil
		}))
	sugar.AppendChild(form, sel)

	for _, c := range(courseNames) {
		option := sugar.CreateElement(document, "option")
		option.Set("value", c)
		option.Set("innerHTML", strings.ToUpper(c))

		sugar.AppendChild(sel, option)
	}

	table := sugar.CreateElement(document, "table")
	table.Set("id", "main-content-table")
	sugar.SetAttribute(table, "class", "booktabs")
	sugar.AppendChild(content, table)

	js.FuncOf(renderGrades).Invoke(student, string(data))
}
