package main

import (
	"flyn.org/git/wasm.git/sugar"
	"fmt"
	"syscall/js"
	"aquinas/wasm"
)

func toggle(x, y, z js.Value, set bool) {
	if set {
		sugar.RemoveAttribute(x, "disabled")
		sugar.RemoveAttribute(y, "disabled")
		sugar.RemoveAttribute(z, "disabled")
	} else {
		sugar.SetAttribute(x, "disabled", "")
		sugar.SetAttribute(y, "disabled", "")
		sugar.SetAttribute(z, "disabled", "")
	}
}

func gitProvider(file, student, ignored string) {
	title := "Change Git provider"
	descr := "Change the Git provider associated with your account"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	gitProvider, gitPath, gitUsername, gitToken, err := wasm.GetGitProvider(student)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	p    := sugar.CreateElement(document, "p")
	text := `You can elect to associate a third-party Git provider with
	         your Aquinas account. In this case, Aquinas will grade project
	         submissions you push to that provider upon request.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)

	p     = sugar.CreateElement(document, "p")
	text  = `Please ensure the Git repositories containing your work are
	         available only to you, your Git provider, and Aquinas. We ask
	         that you not share your work with others, according to the
	         Aquinas user agreement.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)

	form := sugar.CreateElement(document, "form")
	sugar.SetAttribute(form, "action", "/git-provider2")
	sugar.SetAttribute(form, "method", "post")

	sugar.AppendChild(form, sugar.CreateTextNode(document, "Git provider: "))

	provider := sugar.CreateElement(document, "select")
	sugar.SetAttribute(provider, "name", "git-provider")
	sugar.SetAttribute(provider, "onchange", "updateForm()")

	opt := sugar.CreateElement(document, "option")
	sugar.SetAttribute(opt, "value", "aquinas")
	if gitProvider == "aquinas" {
		sugar.SetAttribute(opt, "selected", "")
	}
	label := sugar.CreateTextNode(document, "Aquinas")
        sugar.AppendChild(opt, label)
        sugar.AppendChild(provider, opt)

	opt = sugar.CreateElement(document, "option")
	sugar.SetAttribute(opt, "value", "gitlab")
	if gitProvider == "gitlab" {
		sugar.SetAttribute(opt, "selected", "")
	}
	label = sugar.CreateTextNode(document, "GitLab")
        sugar.AppendChild(opt, label)
        sugar.AppendChild(provider, opt)

	sugar.AppendChild(form, provider)
	sugar.AppendChild(form, sugar.CreateElement(document, "br"))

	sugar.AppendChild(form, sugar.CreateTextNode(document, "https://"))

	username := sugar.CreateElement(document, "input")
        sugar.SetAttribute(username, "type", "text")
        sugar.SetAttribute(username, "name", "git-username")
        sugar.SetAttribute(username, "size", "10")
        sugar.SetAttribute(username, "placeholder", "Username")
        sugar.SetAttribute(username, "value", gitUsername)
        sugar.AppendChild(form, username)

	sugar.AppendChild(form, sugar.CreateTextNode(document, ":"))

	token := sugar.CreateElement(document, "input")
        sugar.SetAttribute(token, "type", "text")
        sugar.SetAttribute(token, "name", "git-token")
        sugar.SetAttribute(token, "size", "20")
        sugar.SetAttribute(token, "placeholder", "Token")
        sugar.SetAttribute(token, "value", gitToken)
        sugar.AppendChild(form, token)

	sugar.AppendChild(form, sugar.CreateTextNode(document, "@gitlab.com/"))

	path := sugar.CreateElement(document, "input")
        sugar.SetAttribute(path, "type", "text")
        sugar.SetAttribute(path, "name", "git-path")
        sugar.SetAttribute(path, "size", "10")
        sugar.SetAttribute(path, "placeholder", "Path")
        sugar.SetAttribute(path, "value", gitPath)
        sugar.AppendChild(form, path)

	sugar.AppendChild(form, sugar.CreateTextNode(document, "/PROJECT"))
	sugar.AppendChild(form, sugar.CreateElement(document, "br"))

	input := sugar.CreateElement(document, "input")
        sugar.SetAttribute(input, "type", "submit")
        sugar.SetAttribute(input, "value", "Submit")
        sugar.AppendChild(form, input)

	switch gitProvider {
	case "gitlab":
		toggle(username, token, path, true)
	default:
		toggle(username, token, path, false)
	}

	js.Global().Set("updateForm", js.FuncOf(func (this js.Value, args []js.Value) interface{} {
		switch provider.Get("value").String() {
		case "gitlab":
			toggle(username, token, path, true)
		default:
			toggle(username, token, path, false)
		}

		return nil
	}))

	sugar.AppendChild(content, form)

	location := sugar.GlobalGet("location")
	origin   := location.Get("origin")

	p     = sugar.CreateElement(document, "p")
	text  = fmt.Sprintf(`Name your projects on GitLab to match the Aquinas
	                     projects they solve. You can request that Aquinas
	                     grade PROJECT by issuing an HTTP GET request for
	                     %s/api/grades-now?project=PROJECT.`,
	                     origin)
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)
}
