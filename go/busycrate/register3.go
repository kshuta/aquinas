package main

import (
	"flyn.org/git/wasm.git/sugar"
	"net/url"
	"aquinas/wasm"
)

func register3(file, student, params string) {
	title := "Register"
	descr := "Register a new Aquinas account"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	student = values.Get("student")
	values.Del("student")

	id, err := wasm.PostRegister(student, values)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	content.Set("innerHTML", "Creating your account. " +
	            "Establishing your Git repositories can take some time. " +
	            "Please wait ...")

	if err := wasm.GetWait(id); err != nil {
                sugar.DisplayLocalErr(document, content, err.Error())
                return
        }

	fragment := `Account established. Please click <a href="/login">here</a>.`
	content.Set("innerHTML", fragment)
}
