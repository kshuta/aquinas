package main

import (
	"flyn.org/git/wasm.git/sugar"
	"errors"
	"net/http"
	"net/url"
	"time"
	"aquinas/wasm"
)

func remove2(file, student, params string) {
	title := "Remove account"
	descr := "PERMANENTLY remove your Aquinas account"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	if values.Get("student") != student {
		sugar.DisplayLocalErr(document, content, "failed student name check")
		return
	}

	if err := wasm.DeleteStudentsNamed(student); err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	errChan := make(chan error, 1)

	req, err := http.NewRequest("GET", "projects", nil)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	req.Header.Set("Authorization", "Basic logout")
	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	go func() {
		resp, err := client.Do(req)
		if err != nil {
			errChan <- err
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode != http.StatusOK {
			errChan <- errors.New("Server returned non-200 status")
		}

		errChan <- nil
	}()

	err = <-errChan
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	window := sugar.GlobalGet("window")
	location := window.Get("location")
	location.Call("replace", "/landing")
}
