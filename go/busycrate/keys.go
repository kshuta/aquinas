package main

import (
	"flyn.org/git/wasm.git/sugar"
	"aquinas/wasm"
)

func keys(file, student, ignored string) {
	title := "Set SSH key(s)"
	descr := "Set the public SSH key(s) associated with your Aquinas account"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	keys, err := wasm.GetKeys(student)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	p    := sugar.CreateElement(document, "p")
	text := `Before you can complete your first project, you must provide
	         Aquinas with your public SSH key. This key will prove your
	         identity when you submit your work. The key you provide here
	         must be encoded in the format used by OpenSSH.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)

	p     = sugar.CreateElement(document, "p")
	text  = `Aquinas provides a virtual machine that you can use to
		 complete your projects, or you might have access to a
		 remote computer that allows you to do the same. Follow
		 <a href="/project/computer"> these directions</a> to
		 download, install, and run the virtual machine. Follow
		 <a href="/project/ssh">these directions</a> to
		 connect to a remote host.  Once you have access to a
		 sufficiently-equipped terminal, generate an OpenSSH
		 keypair by running <code>ssh-keygen</code>.  Accept the
		 default storage location when prompted. If you choose
		 to use a non-blank passphrase, then you will need to
		 enter that passphrase each time you submit a project
		 to Aquinas.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)

	p     = sugar.CreateElement(document, "p")
	text  = `(If you use a different SSH client, then it might be necessary
	         to transform its key into the OpenSSH format before
	         submitting the result. You will need to look elsewhere for
	         instructions on how to perform such a transformation.)`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)

	p     = sugar.CreateElement(document, "p")
	text  = `Once you have a key, you must enter it in the form below.
	         Print your key using <code>cat ~/.ssh/id_rsa.pub</code>, and copy and
	         paste it here before pressing the submit button.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)

	form := sugar.CreateElement(document, "form")
	sugar.SetAttribute(form, "action", "/keys2")
	sugar.SetAttribute(form, "method", "post")
	sugar.AppendChild(content, form)

	textarea := sugar.CreateElement(document, "textarea")
	sugar.SetAttribute(textarea, "rows", "6")
	sugar.SetAttribute(textarea, "cols", "80")
	sugar.SetAttribute(textarea, "name", "key")
	sugar.SetAttribute(textarea, "placeholder", "SSH key")
	sugar.AppendChild(textarea, sugar.CreateTextNode(document, keys))
	sugar.AppendChild(form, textarea)

	br := sugar.CreateElement(document, "br")
	sugar.AppendChild(form, br)

	input := sugar.CreateElement(document, "input")
	sugar.SetAttribute(input, "type", "submit")
	sugar.SetAttribute(input, "value", "Submit")
	sugar.AppendChild(form, input)
}
