package main

import (
	"flyn.org/git/wasm.git/sugar"
	"encoding/json"
	"fmt"
	"aquinas/proj"
	"strings"
	"syscall/js"
	"aquinas/wasm"
)

func renderBook(this js.Value, args []js.Value) interface{} {
	data := args[0].String()

	document := sugar.GlobalGet("document")
	ul := sugar.GetElementByID(document, "main-content-ul")
	ul.Set("innerHTML", "")

	input := sugar.GetElementByID(document, "main-content-search")
	search := input.Get("value").String()

	grades := []proj.Grade{}
	json.Unmarshal([]byte(data), &grades)

	for _, g := range grades {
		if !strings.Contains(g.Student, search) &&
		   !strings.Contains(g.Project, search) &&
		   !strings.Contains(g.Outcome, search) &&
		   !strings.Contains(g.Timestamp, search) {
			continue
		}

		li := sugar.CreateElement(document, "li")
		record := fmt.Sprintf("%s: %s %s %s", g.Student, g.Project, g.Outcome, g.Timestamp)
		sugar.AppendChild(li, sugar.CreateTextNode(document, record))
		sugar.AppendChild(ul, li)
	}

	return nil
}

func book(file, student, ignored string) {
	title := "Grade book"
	descr := "List of student grades"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	grades, err := wasm.GetGradesBrief()
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	data, err := json.Marshal(grades)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	label := sugar.CreateElement(document, "label")
	label.Set("innerHTML", "Search:")
	sugar.AppendChild(content, label)

	input := sugar.CreateElement(document, "input")
	input.Set("type", "text")
	input.Set("placeholder", "String")
	input.Set("id", "main-content-search")
	sugar.AddEventListener(input, "input", js.FuncOf(
		func(this js.Value, args []js.Value) interface{} {
			js.FuncOf(renderBook).Invoke(string(data))
			return nil
		}))
	sugar.AppendChild(content, input)

	ul := sugar.CreateElement(document, "ul")
	ul.Set("id", "main-content-ul")
	sugar.AppendChild(content, ul)

	js.FuncOf(renderBook).Invoke(string(data))
}
