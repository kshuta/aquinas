package main

import (
	"flyn.org/git/wasm.git/sugar"
	"aquinas/wasm"
)

func alias(file, student, ignored string) {
	title := "Change alias"
	descr := "Change the public alias others will know you by"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	alias, err := wasm.GetAlias(student)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	p    := sugar.CreateElement(document, "p")
	text := `Aquinas uses your alias when reporting statistics such as
	         rankings to other students. This avoids sharing your email
	         address, and it puts you in control of whether these reports
	         correlate your performance with your real-world identity.
	         Teachers are able to identify students by their email
	         addresses, but Aquinas does not share this information
	         publicly.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)

	p     = sugar.CreateElement(document, "p")
	text  = `Please use discretion when selecting your alias,
	         and be respectful of the sentiments of your fellow students.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)

	form := sugar.CreateElement(document, "form")
	sugar.SetAttribute(form, "action", "/alias2")
	sugar.SetAttribute(form, "method", "post")
	sugar.AppendChild(content, form)

	sugar.AppendChild(form, sugar.CreateTextNode(document, "Alias: "))

	input := sugar.CreateElement(document, "input")
	sugar.SetAttribute(input, "type", "text")
	sugar.SetAttribute(input, "name", "alias")
	sugar.SetAttribute(input, "placeholder", "Alias")
	sugar.SetAttribute(input, "value", alias)
	sugar.AppendChild(form, input)

	input  = sugar.CreateElement(document, "input")
	sugar.SetAttribute(input, "type", "submit")
	sugar.SetAttribute(input, "value", "Submit")
	sugar.AppendChild(form, input)
}
