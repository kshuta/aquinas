package main

import (
	"flyn.org/git/wasm.git/sugar"
	"fmt"
	"sort"
	"aquinas/wasm"
)

func rankings(file, student, ignored string) {
	title := "Rankings"
	descr := "Student rankings"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	ranks, err := wasm.GetRanks()
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	keys := []int{}
	for k := range ranks {
		keys = append(keys, k)
	}
	sort.Sort(sort.Reverse(sort.IntSlice(keys)))

	gradeList := sugar.CreateElement(document, "ol")

	for _, k := range keys {
		plural := ""
		if k != 1 {
			plural = "s"
		}
		entry := sugar.CreateElement(document, "li")
		e := fmt.Sprintf("%s with %d solution%s.", renderList(ranks[k]), k, plural)
		sugar.AppendChild(entry, sugar.CreateTextNode(document, e))
		sugar.AppendChild(gradeList, entry)
	}

	sugar.AppendChild(content, gradeList)
}
