package main

import (
	"flyn.org/git/wasm.git/sugar"
	"net/url"
	"aquinas/wasm"
)

func reset3(file, student, params string) {
	title := "Reset password"
	descr := "Reset the password associated with your Aquinas account"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	student = values.Get("student")
	values.Del("student")

	if err := wasm.PostResetPassword(student, values); err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	window := sugar.GlobalGet("window")
	location := window.Get("location")
	location.Call("replace", "/login")

	p    := sugar.CreateElement(document, "p")
	text := `Please <a href="/login">click here</a>.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)
}
