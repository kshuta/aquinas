package main

import (
	"flyn.org/git/wasm.git/sugar"
	"net/url"
	"flyn.org/git/safe.git"
	"aquinas/student"
	"aquinas/wasm"
)

func alias2(file, email, params string) {
	alias := new(safe.String)

	title := "Change alias"
	descr := "Change the public alias others will know you by"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	if _, ok := values["alias"]; !ok {
		sugar.DisplayLocalErr(document, content, "missing alias value")
		return
	}

	if err := alias.UnmarshalText([]byte(values.Get("alias"))); err != nil {
		sugar.DisplayLocalErr(document, content, "ill-formed alias")
		return
	}

	if err := wasm.PostStudent(email, student.Student{Alias: alias}); err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
	}

	p    := sugar.CreateElement(document, "p")
	text := `Alias changed. Please <a href="/">click</a> to continue.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)
}
