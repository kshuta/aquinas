package main

import (
	"flyn.org/git/wasm.git/sugar"
	"syscall/js"
	"github.com/google/uuid"
	"aquinas/wasm"
)

func fork(this js.Value, args []js.Value) interface{} {
	project := args[0].String()

	errChan := make(chan error, 1)

	go func() {
		id, err := wasm.PostFork(project)
		if err != nil {
			errChan <- err
			return
		}

		if err := wasm.GetWait(id); err != nil {
			errChan <- err
			return
		}

		errChan <- nil
	}()

	go func() {
		err := <-errChan
		if err != nil {
			document := sugar.GlobalGet("document")
			content  := sugar.GetElementsByClassName(document, "main-content").Index(0)
			sugar.DisplayLocalErr(document, content, err.Error())
		} else {
			sugar.Reload()
		}
	}()

	return nil
}

func reloadWhenGraded(jobUUID uuid.UUID) {
	errChan := make(chan error, 1)

	go func() {
		if err := wasm.GetWait(jobUUID.String()); err != nil {
			errChan <- err
			return
		}

		errChan <- nil
	}()

	go func() {
		err := <-errChan
		if err != nil {
			document := sugar.GlobalGet("document")
			content  := sugar.GetElementsByClassName(document, "main-content").Index(0)
			sugar.DisplayLocalErr(document, content, err.Error())
		} else {
			sugar.Reload()
		}
	}()
}

func gradenow(this js.Value, args []js.Value) interface{} {
	project := args[0].String()

	go func() {
		wasm.GetGradesNow(project)
		sugar.Reload()
	}()

	return nil
}

func project(file, student, ignored string) {
	document, content := sugar.DisplayStart()

	inst, err := wasm.GetProjectsNamed(file)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	sugar.DisplaySetTitleDescr(document, "Aquinas", inst.Title, inst.Summary)
	content.Set("innerHTML", inst.Instructions)

	forkLink := sugar.GetElementByID(document, "fork-link")
	if !forkLink.IsNull() {
		sugar.AppendChild(forkLink, sugar.CreateTextNode(document, "Before beginning this project, you will need to "))
		link := sugar.CreateElement(document, "a")
		sugar.SetAttribute(link, "href", "")
		sugar.AppendChild(link, sugar.CreateTextNode(document, "fork"))
		sugar.AddEventListener(link, "click", js.FuncOf(
			func(this js.Value, args []js.Value) interface{} {
				/* Prevent natural page load, which would cancel loads in callback. */
				args[0].Call("preventDefault")
				return js.FuncOf(fork).Invoke(file)
			}))
		sugar.AppendChild(forkLink, link)
		sugar.AppendChild(forkLink, sugar.CreateTextNode(document, " the canonical project repository."))
	}

	table := sugar.GetElementByID(document, "record-table")
	if !table.IsNull() {
		grading := false

		jobUUID, err := wasm.GetIsGrading(file)
		if jobUUID != uuid.Nil {
			grading = true
		}

		grades, err := wasm.GetGradesStudentProject(student, file)
		if err != nil {
			sugar.DisplayLocalErr(document, content, err.Error())
			return
		}

		for i, g := range grades {
			tr := sugar.CreateElement(document, "tr")
			if i == len(grades) - 1 {
				sugar.SetAttribute(tr, "class", "booktabs-bottom")
			}

			td := sugar.CreateElement(document, "td")
			sugar.SetAttribute(td, "class", "booktabs")
			sugar.AppendChild(td, sugar.CreateTextNode(document, g.Commit))
			sugar.AppendChild(tr, td)

			td = sugar.CreateElement(document, "td")
			sugar.SetAttribute(td, "class", "booktabs")
			sugar.AppendChild(td, sugar.CreateTextNode(document, g.Timestamp))
			sugar.AppendChild(tr, td)

			td = sugar.CreateElement(document, "td")
			sugar.SetAttribute(td, "class", "booktabs")
			sugar.AppendChild(td, sugar.CreateTextNode(document, g.Outcome))
			sugar.AppendChild(tr, td)

			sugar.AppendChild(table, tr)
		}

		parent := sugar.ParentElement(table)

		if grading {
			p := sugar.CreateElement(document, "p")
			p.Set("innerHTML", "Currently grading ...")
			sugar.AppendChild(parent, p)

			reloadWhenGraded(jobUUID)
		} else {
			js.Global().Set("gradenow", js.FuncOf(func (this js.Value, args[]js.Value) interface{} {
				return js.FuncOf(gradenow).Invoke(file)
			}))

			button := sugar.CreateElement(document, "input")
			sugar.SetAttribute(button, "type", "button")
			sugar.SetAttribute(button, "onclick", "gradenow()")
			sugar.SetAttribute(button, "value", "Regrade")
			sugar.AppendChild(parent, button)
		}
	}

	js.Global().Get("MathJax").Call("typeset")
}
