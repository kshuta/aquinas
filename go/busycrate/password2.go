package main

import (
	"flyn.org/git/wasm.git/sugar"
	"net/url"
	"flyn.org/git/safe.git"
	"aquinas/student"
	"aquinas/wasm"
)

func password2(file, email, params string) {
	password := new(safe.String)

	title := "Change password"
	descr := "Change the password associated with your Aquinas account"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	if values.Get("password") != values.Get("password2") {
		sugar.DisplayLocalErr(document, content, "Passwords do not match")
		return
	}

	if err := password.UnmarshalText([]byte(values.Get("password"))); err != nil {
		sugar.DisplayLocalErr(document, content, "ill-formed password")
		return
	}

	if err = wasm.PostStudent(email, student.Student{
		Password: password,
	}); err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	p := sugar.CreateElement(document, "p")
	text := `Password changed. Please <a href="/">click</a> to continue.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)
}
