package main

import (
	"flyn.org/git/wasm.git/sugar"
	"flyn.org/git/safe.git"
	"strings"
	"aquinas/student"
	"aquinas/wasm"
)

func keys2(file, email, params string) {
	keys := new(safe.Key)

	title := "Set SSH Key(s)"
	descr := "Set the public SSH key(s) associated with your Aquinas account"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	/*
	 * Avoid HTTP Get and url.ParseQuery to avoid replacing '+' with ' '.
	 * Body of key (after first '=') might contain '=', but we restore these
	 * with the Join below.
	 */
	split := strings.Split(params, "=")
	if len(split) < 2 {
		sugar.DisplayLocalErr(document, content, "bad query: " + params)
		return
	}

	s := strings.Join(split[1:], "=")

	if err := keys.UnmarshalText([]byte(s)); err != nil {
                sugar.DisplayLocalErr(document, content, "ill-formed key")
                return
        }

	if err := wasm.PostStudent(email, student.Student{Key: keys}); err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	p    := sugar.CreateElement(document, "p")
	text := `Key installed. Please <a href="/">click</a> to continue.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)
}
