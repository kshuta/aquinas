package main

import (
	"flyn.org/git/wasm.git/sugar"
	"fmt"
	"net/url"
	"aquinas/wasm"
)

func register2(file, student, params string) {
	title := "Register"
	descr := "Register a new Aquinas account"
	document, content := sugar.DisplayStart()

	sugar.DisplaySetTitleDescr(document, "Aquinas", title, descr)

	values, err := url.ParseQuery(params)
	if err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	student = values.Get("student")

	if err := wasm.GetRegister(student, params); err != nil {
		sugar.DisplayLocalErr(document, content, err.Error())
		return
	}

	p    := sugar.CreateElement(document, "p")
	text := fmt.Sprintf("We have received your request, %s.", student)
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)

	p     = sugar.CreateElement(document, "p")
	text  = `Please watch for an email containing your password and
	         instructions that describe how to complete
	         your registration.`
	p.Set("innerHTML", text)
	sugar.AppendChild(content, p)
}
