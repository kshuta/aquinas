package main

import (
	"aquinas/common"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path"
	"strconv"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Fprintf(os.Stderr, "usage: %s USER KEY\n", os.Args[0])
		os.Exit(1)
	}

	norm := common.NormalizeUsername(os.Args[1])
	key  := os.Args[2]

	u, err := user.Lookup(norm)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not lookup user %s: %s\n", norm, err)
		os.Exit(1)
	}

	uid, err := strconv.Atoi(u.Uid)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not parse UID %s: %s\n", u.Uid, err)
		os.Exit(1)
	}

	gid, err := strconv.Atoi(u.Uid)
	if err != nil {
		fmt.Fprintf(os.Stderr, "could not parse GID %s: %s\n", u.Gid, err)
		os.Exit(1)
	}

	/* Create .ssh directory if it does not exists. */
	sshDir := path.Join(u.HomeDir, ".ssh")
	if _, err := os.Stat(sshDir); os.IsNotExist(err) {
		if err := os.Mkdir(sshDir, 0700); err != nil {
			fmt.Fprintf(os.Stderr, "unable to create %s: %s\n", sshDir, err)
			os.Exit(1)
		}

		if err := os.Chown(sshDir, uid, gid); err != nil {
			fmt.Fprintf(os.Stderr, "unable set ownership of %s: %s\n", sshDir, err)
			os.Exit(1)
		}
	}

	f := path.Join(sshDir, "authorized_keys")
	if err := ioutil.WriteFile(f, []byte(key), 0600); err != nil {
		fmt.Fprintf(os.Stderr, "unable to write key to %s: %s\n", f, err)
		os.Exit(1)
	}

	if err := os.Chown(f, uid, gid); err != nil {
		fmt.Fprintf(os.Stderr, "unable to set ownership of %s: %s\n", f, err)
		os.Exit(1)
	}
}
