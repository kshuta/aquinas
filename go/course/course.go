package course

// Event represents a graded event.
type Event struct {
	Name string  `json:"name"`
	Max  float64 `json:"max"`
}

// Course represents a course.
type Course struct {
	Name         string `json:"name"`
	Events     []Event  `json:"events"`
	Enrollment []string `json:"enrollment"`
}
