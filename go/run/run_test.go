package run

import (
	"aquinas/conf"
	"strings"
	"testing"
	"time"
)

func TestStandardStdout(t *testing.T) {
	output, _, err := Standard(nil, nil, "echo", "foo")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestStandardStdin(t *testing.T) {
	stdin := strings.NewReader("foo\n")
	output, _, err := Standard(stdin, nil, "cat")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestStandardStderr(t *testing.T) {
	_, _, err := Standard(nil, nil, "ls", "/file-does-not-exist")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: cannot access '/file-does-not-exist': No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestStandardStderrExitZero(t *testing.T) {
	_, emsg, err := Standard(nil, nil, "sh", "-c", "echo foo >&2")
	if err != nil {
		t.Error(err.Error())
	}
	if string(emsg) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(emsg))
	}
}

func TestStandardExitCode(t *testing.T) {
	_, _, err := Standard(nil, nil, "false")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*Error); ok {
		if stderrErr.ExitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}

func TestStandardBadCommand(t *testing.T) {
	_, _, err := Standard(nil, nil, "command-does-not-exist")
	if err == nil {
		t.Error("non-existing command did not produce error")
	}
}

func TestSplit(t *testing.T) {
	s, err := split(`foo "bar baz"`)
	if err != nil {
		t.Error("unexpected value: " + err.Error())
	}
	if s[0] != "foo" {
		t.Error("unexpected value: " + s[0])
	}
	if s[1] != "bar baz" {
		t.Error("unexpected value: " + s[1])
	}
}

func TestSplitDash(t *testing.T) {
	s, err := split(`foo -bar`)
	if err != nil {
		t.Error("unexpected value: " + err.Error())
	}
	if s[0] != "foo" {
		t.Error("unexpected value: " + s[0])
	}
	if s[1] != "-bar" {
		t.Error("unexpected value: " + s[1])
	}
}

func TestSimpleStdout(t *testing.T) {
	output, _, err := Simple(nil, nil, `echo "foo bar"`)
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo bar\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestTimedStdout(t *testing.T) {
	output, _, err := Timed(1 * time.Second, nil, nil, "echo", "foo")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestTimedStdin(t *testing.T) {
	stdin := strings.NewReader("foo\n")
	output, _, err := Timed(1 * time.Second, stdin, nil, "cat")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in output: " + string(output))
	}
}

func TestTimedStderr(t *testing.T) {
	_, _, err := Timed(1 * time.Second, nil, nil, "ls", "/file-does-not-exist")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: cannot access '/file-does-not-exist': No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestTimedStderrExitZero(t *testing.T) {
	_, emsg, err := Timed(1 * time.Second, nil, nil, "sh", "-c", "echo foo >&2")
	if err != nil {
		t.Error(err.Error())
	}
	if string(emsg) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(emsg))
	}
}

func TestTimedExitCode(t *testing.T) {
	_, _, err := Timed(1 * time.Second, nil, nil, "false")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*Error); ok {
		if stderrErr.ExitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}

func TestTimedBadCommand(t *testing.T) {
	_, _, err := Timed(1 * time.Second, nil, nil, "command-does-not-exist")
	if err == nil {
		t.Error("non-existing command did not produce error")
	}
}

func TestTimedOut(t *testing.T) {
	_, _, err := Timed(1 * time.Second, nil, nil, "sleep", "2")
	switch err {
	case ErrTimeout:
		return
	case nil:
		t.Error("process did not return error; should have timed out")
	default:
		t.Error("should have timed out, but received:" + err.Error())
	}
}

func TestRemoteStdout(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	output, _, err := Remote(nil, nil, "root@" + conf.HostGit() + "." + conf.Domain(), "echo", "foo")
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(output))
	}
}

func TestRemoteStderr(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	_, _, err := Remote(nil, nil, "root@" + conf.HostGit() + "." + conf.Domain(), "ls", "/file-does-not-exist")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: /file-does-not-exist: No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestRemoteFail(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	_, _, err := Remote(nil, nil, "root@" + conf.HostGit() + "." + conf.Domain(), "false")
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*Error); ok {
		if stderrErr.ExitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}

func TestSSHStdout(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	stdin := strings.NewReader("echo foo\n")
	output, _, err := SSH(stdin, nil, "root@" + conf.HostGit() + "." + conf.Domain())
	if err != nil {
		t.Error(err.Error())
	}
	if string(output) != "foo\n" {
		t.Error("unexpected value in stderr: " + string(output))
	}
}

func TestSSHStderr(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	stdin := strings.NewReader("ls /file-does-not-exist")
	_, _, err := Remote(stdin, nil, "root@" + conf.HostGit() + "." + conf.Domain())
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if "ls: /file-does-not-exist: No such file or directory\n" != err.Error() {
		t.Error("unexpected value in stderr: " + err.Error())
	}
}

func TestSSHFail(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	stdin := strings.NewReader("false")
	_, _, err := SSH(stdin, nil, "root@" + conf.HostGit() + "." + conf.Domain())
	if err == nil {
		t.Error("expected error but did not receive one")
	}
	if stderrErr, ok := err.(*Error); ok {
		if stderrErr.ExitCode != 1 {
			t.Error("received wrong exit code")
		}
	} else {
		t.Error(err.Error())
	}
}
