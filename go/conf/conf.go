package conf

import (
	"encoding/json"
	"os"
	"regexp"
	"runtime"
)

type configuration struct {
	Domain string          `json:"domain"`
	HostGit string         `json:"hostGit"`
	HostWWW string         `json:"hostWWW"`
	HostUser string        `json:"hostUser"`
	HostTarget string      `json:"hostTarget"`
	ConfRoot string        `json:"confRoot"`
	DataRoot string        `json:"dataRoot"`
	EmailRegex string      `json:"emailRegex"`
	EmailRelay string      `json:"emailRelay"`
	EmailSender string     `json:"emailSender"`
	EmailSenderName string `json:"emailSenderName"`
}

// SockPath is the path to the socket used by queued.
const SockPath = "/tmp/queue-socket"

var conf *configuration

func init() {
	if conf == nil {
		conf = new(configuration)

		if runtime.GOOS == "js" {
			// Cannot use os.Open.
			return
		}

		f, err := os.Open("aquinas.json")
		if err != nil {
			f, err = os.Open("../../aquinas.json")
		}
		if err != nil {
			if f, err = os.Open("/etc/aquinas/aquinas.json"); err != nil {
				panic(err)
			}
		}

		defer f.Close()

		dec := json.NewDecoder(f)
		if err = dec.Decode(conf); err != nil {
			panic(err)
		}
	}

	/* Ensure regex is well-formed. */
	regexp.MustCompile(conf.EmailRegex)
}

// Domain returns the configured domain.
func Domain() string {
	return conf.Domain
}

// HostGit returns the configured Git host.
func HostGit() string {
	return conf.HostGit
}

// HostWWW returns the configured WWW host.
func HostWWW() string {
	return conf.HostWWW
}

// HostUser returns the configured user host.
func HostUser() string {
	return conf.HostUser
}

// HostTarget returns the configured target host.
func HostTarget() string {
	return conf.HostTarget
}

// ConfRoot returns the configured filesystem configuration root.
func ConfRoot() string {
	return conf.ConfRoot
}

// DataRoot returns the configured filesystem data root.
func DataRoot() string {
	return conf.DataRoot
}

// EmailRegex returns the regular expression that specifies permitted email
// domains.
func EmailRegex() string {
	return conf.EmailRegex
}

// EmailRelay returns the host that serves as Aquinas' email relay.
func EmailRelay() string {
	return conf.EmailRelay
}

// EmailSender returns the email address that serves as the from address for
// Aquinas emails.
func EmailSender() string {
	return conf.EmailSender
}

// EmailSenderName returns the name that serves as the from name for
// Aquinas emails.
func EmailSenderName() string {
	return conf.EmailSenderName
}
