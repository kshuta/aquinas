package main

/*
 * This program updates a user's records on the Aqunas WWW server. It is
 * meant to be a shell associated with the teacher account.
 */

import (
	"bufio"
	"aquinas/common"
	"aquinas/conf"
	"aquinas/proj"
	"encoding/json"
	"fmt"
	"golang.org/x/sys/unix"
	"io"
	"io/ioutil"
	"log/syslog"
	"os"
	"path"
	"strings"
)

var logger *syslog.Writer

func main() {
	var err error

	logger, err = syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		panic(err)
	}

	reader := bufio.NewReader(os.Stdin)

	command, err := reader.ReadString('\n')
	if err != nil {
		common.Fail(logger.Err, fmt.Errorf("could not read command: %s", err))
	}
	command = strings.TrimSuffix(command, "\n")

	switch command {
	case "record":
		err = handleRecord(reader)
	case "binary":
		err = handleBinary(reader)
	default:
		err = fmt.Errorf("unknown command: %s", command)
	}

	if err != nil {
		common.Fail(logger.Err, err)
	}
}

func handleRecord(reader *bufio.Reader) error {
	var grades []proj.Grade

	gradesS, err := reader.ReadString('\n')
	if err != nil {
		return fmt.Errorf("could not read records: %s", err)
	}

	dec := json.NewDecoder(strings.NewReader(gradesS))

	if err := dec.Decode(&grades); err != nil {
		return fmt.Errorf("could decode grades: %s", err)
	}

	student := grades[0].Student
	project := grades[0].Project

	unix.Umask(0)

	dir := path.Join(conf.DataRoot(), "/www", student)
	if _, err := os.Stat(dir); err != nil {
		if os.IsNotExist(err) {
			if err := os.Mkdir(dir, 0774 | os.ModeSetgid); err != nil {
				return fmt.Errorf("could not create %s: %s", dir, err)
			}
		} else {
			return fmt.Errorf("could not stat %s: %s", dir, err)
		}
	}

	recordPath := path.Join(dir, project + "-records")
	if err := ioutil.WriteFile(recordPath, []byte(gradesS), 0640); err != nil {
		return fmt.Errorf("could not write to %s: %s", recordPath, err)
	}

	lastPath := path.Join(dir, project + "-last")

	f, err := os.OpenFile(lastPath, os.O_WRONLY | os.O_CREATE | os.O_TRUNC, 0660)
	if err != nil {
		return fmt.Errorf("could not open %s: %s", lastPath, err)
	}

	defer f.Close()

	for {
		last, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			return fmt.Errorf("could not read last: %s", err)
		}

		fmt.Fprintf(f, "%s", last)
	}

	return nil
}

func handleBinary(reader *bufio.Reader) error {
	proj, err := reader.ReadString('\n')
	if err != nil {
		return fmt.Errorf("could not read project name: %s", err)
	}
	proj = strings.TrimSuffix(proj, "\n")

	filename, err := reader.ReadString('\n')
	if err != nil {
		return fmt.Errorf("could not read filename: %s", err)
	}
	filename = strings.TrimSuffix(filename, "\n")

	dir := path.Join(conf.DataRoot(), "/www", "project", proj, "bin")

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.Mkdir(dir, 0750)
		if err != nil {
			return fmt.Errorf("could not mkdir %s: %s", dir, err)
		}
	}

	binPath := path.Join(dir, filename)
	f, err := os.OpenFile(binPath, os.O_WRONLY | os.O_CREATE | os.O_TRUNC, 0750)
	if err != nil {
		return fmt.Errorf("could not open %s: %s", binPath, err)
	}

	defer f.Close()

	if _, err := io.Copy(f, reader); err != nil {
		return fmt.Errorf("could copy binary to %s: %s", binPath, err)
	}

	return nil
}
