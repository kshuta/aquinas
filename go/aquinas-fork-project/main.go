package main

import (
	"aquinas/common"
	"aquinas/conf"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log/syslog"
	"net/http"
	"net/url"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"aquinas/proj"
	"aquinas/run"
	"strconv"
	"strings"
)

func main() {
	var student, projectAbstractName, language string
	var repo common.GitRepository

	logger, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		panic(err)
	}

	if len(os.Args) != 4 && len(os.Args) != 5 {
		fmt.Fprintf(os.Stderr, "usage: %s USER PROJECT GIT-JSON [LANGUAGE]\n", os.Args[0])
		os.Exit(1)
	}

	student = os.Args[1]
	projectAbstractName = os.Args[2]

	if err := json.Unmarshal([]byte(os.Args[3]), &repo); err != nil {
		fmt.Fprintf(os.Stderr, "could not unmarshal %s: %s", os.Args[3], err)
		os.Exit(1)
	}

	if len(os.Args) == 5 {
		language = os.Args[4]
	}

	projects, err := updateTeacherProject()
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to update teacher project: %s\n", err)
		os.Exit(1)
	}

	f, err := os.Open(path.Join(projects, projectAbstractName, "description.json"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to open: %s\n", err)
		os.Exit(1)
	}
	defer f.Close()

	project, err := proj.DecodeAndInstantiateProject(f, language)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to decode: %s\n", err)
		os.Exit(1)
	}

	if len(project.Checks()) == 0 {
		os.Exit(0)
	}

	switch repo.Provider {
	case "gitlab":
		logger.Debug("forking " + project.Name() + " to GitLab repo for " + student)

		if err := forkGitLab(projects, project, student, repo); err != nil {
			fmt.Fprintf(os.Stderr, "failed to fork %s: %s\n", project.Name(), err)
			os.Exit(1)
		}
	default:
		logger.Debug("forking " + project.Name() + " to Aquinas repo for " + student)

		if err := forkAquinas(projects, project, student); err != nil {
			fmt.Fprintf(os.Stderr, "failed to fork %s: %s\n", project.Name(), err)
			os.Exit(1)
		}
	}
}

func updateTeacherProject() (string, error) {
	var cmd string

	t, err := user.Lookup("teacher")
	if err != nil {
		return "", fmt.Errorf("could not lookup user teacher: %s", err)
	}

	/* TODO: Consolidate this work elsewhere? */
	/* Make sure teacher's workdir exists. */
	workdir := path.Join(t.HomeDir, "workdir")
	if _, err := os.Stat(workdir); os.IsNotExist(err) {
		if err := os.Mkdir(workdir, 0700); err != nil {
			return "", fmt.Errorf("unable to create %s: %s", workdir, err)
		}

		uid, err := strconv.Atoi(t.Uid)
		if err != nil {
			return "", fmt.Errorf("could not parse UID %s: %s", t.Uid, err)
		}

		gid, err := strconv.Atoi(t.Gid)
		if err != nil {
			return "", fmt.Errorf("could not parse GID %s: %s", t.Gid, err)
		}

		if err := os.Chown(workdir, uid, gid); err != nil {
			return "", fmt.Errorf("unable set ownership of %s: %s", workdir, err)
		}
	}

	projects := path.Join(workdir, "projects")
	if _, err := os.Stat(projects); os.IsNotExist(err) {
		repo := path.Join(t.HomeDir, "projects")
		cmd = fmt.Sprintf("sudo -u teacher git clone -q %s %s", repo, projects)
	} else {
		cmd = fmt.Sprintf("sudo -u teacher git -C %s pull -q", projects)
	}
	if _, _, err := run.Simple(nil, nil, cmd); err != nil {
		return "", fmt.Errorf("failed to run %s: %s", cmd, err)
	}

	return projects, nil
}

func forkAquinas(teacherdir string, project proj.ProjectInterface, student string) error {
	norm := common.NormalizeUsername(student)

	u, err := user.Lookup(norm)
	if err != nil {
		return fmt.Errorf("could not lookup user %s: %s", norm, err)
	}

	if _, err := os.Stat(path.Join(u.HomeDir, project.Name())); err == nil {
		// Project already exists.
		return nil
	}

	uid, err := strconv.Atoi(u.Uid)
	if err != nil {
		return fmt.Errorf("could not parse UID %s: %s", u.Uid, err)
	}

	gid, err := strconv.Atoi(u.Gid)
	if err != nil {
		return fmt.Errorf("could not parse GID %s: %s", u.Gid, err)
	}

	t, err := user.Lookup("teacher")
	if err != nil {
		return fmt.Errorf("could not lookup user teacher: %s", err)
	}

	gidTeacher, err := strconv.Atoi(t.Gid)
	if err != nil {
		return fmt.Errorf("could not parse GID %s: %s", u.Gid, err)
	}

	if err := updateKnownHosts(u.HomeDir, uid, gid); err != nil {
		return err
	}

	projdir, err := gitInit(u.HomeDir, project)
	if err != nil {
		return err
	}

	if project.Template() != "" {
		if err := addTemplates(teacherdir, projdir, project, uid, gid); err != nil {
			return err
		}
	}

	// Set hooks after adding template to avoid spurious grading.
	if err := sethooks(projdir); err != nil {
		return fmt.Errorf("failed setup git hooks: %s", err)
	}

	if err := filepath.Walk(projdir, func(path string, info os.FileInfo, err error) error {
		var perm os.FileMode

		if err != nil {
			return err
		}

		if info.Mode() & os.ModeSymlink == os.ModeSymlink {
			// Do not touch symbolic links (e.g., Git hooks).
			return nil
		} else if info.IsDir() {
			perm = 0775 | os.ModeSetgid
		} else {
			perm = 0664
		}

		if err = os.Chmod(path, perm); err != nil {
			return err
		}

		return os.Chown(path, uid, gidTeacher)
	}); err != nil {
		return err
	}

	return nil
}

func forkGitLab(teacherdir string, project proj.ProjectInterface, student string, repo common.GitRepository) error {
	norm := common.NormalizeUsername(student)

	u, err := user.Lookup(norm)
	if err != nil {
		return fmt.Errorf("could not lookup user %s: %s", norm, err)
	}

	uid, err := strconv.Atoi(u.Uid)
	if err != nil {
		return fmt.Errorf("could not parse UID %s: %s", u.Uid, err)
	}

	gid, err := strconv.Atoi(u.Gid)
	if err != nil {
		return fmt.Errorf("could not parse GID %s: %s", u.Gid, err)
	}

	if err := updateKnownHosts(u.HomeDir, uid, gid); err != nil {
		return err
	}

	if err := createGitlab(project.Name(), student, repo); err != nil {
		return err
	}

	r := common.BuildRepo("", project.Name(), repo.Provider, repo.Path, repo.Username, repo.Token)
	if project.Template() != "" {
		if err := addTemplates(teacherdir, r, project, uid, gid); err != nil {
			return err
		}
	}

	return nil
}

func createGitlab(project, student string, repo common.GitRepository) error {
	u       := fmt.Sprintf("https://gitlab.com/api/v4/projects?private_token=%s", repo.Token)
	values  := url.Values{
		"name": {project},
		"description": {"Solution for Aquinas project " + project},
		"visibility": {"private"},
	}

	resp, err := http.PostForm(u, values)
	if err != nil {
		return fmt.Errorf("could not create GitLab repository: %s", err)
	}

	defer resp.Body.Close()

	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("could not read response from GitLab repository: %s", err)
	}

	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("server returned non-201 status: %d", resp.StatusCode)
	}

	return nil
}

func duplicate(dst, src string, uid, gid int) error {
	i, err := os.Stat(src)
	if err != nil {
		return err
	}

	if i.IsDir() {
		err := os.Mkdir(dst, i.Mode())
		if err != nil {
			return err
		}
	} else {
		s, err := os.Open(src)
		if err != nil {
			return err
		}
		defer s.Close()

		d, err := os.Create(dst)
		if err != nil {
			return err
		}
		defer d.Close()

		err = os.Chmod(dst, i.Mode())
		if err != nil {
			return err
		}

		_, err = io.Copy(d, s)
	}

	err = os.Chown(dst, uid, gid)
	if err != nil {
		return err
	}

	return err
}

func gitInit(homedir string, project proj.ProjectInterface) (string, error) {
	projdir := path.Join(homedir, project.Name())
	if _, err := os.Stat(projdir); os.IsNotExist(err) {
		if err := os.Mkdir(projdir, 0750); err != nil {
			return "", fmt.Errorf("unable to create %s: %s", projdir, err)
		}
	}

	cmd := fmt.Sprintf("git init -q --bare %s", projdir)
	if _, _, err := run.Simple(nil, nil, cmd); err != nil {
		return "", fmt.Errorf("failed to run %s: %s", cmd, err)
	}

	return projdir, nil
}

func addTemplates(teacherdir, repo string, project proj.ProjectInterface, uid, gid int) error {
	var dst string

	tmpdir, err := ioutil.TempDir("", "aquinas-fork-project")
	if err != nil {
		return fmt.Errorf("could not create temporary directory: %s", err)
	}

	defer os.RemoveAll(tmpdir)

	cmd := fmt.Sprintf("git clone -q %s %s", repo, tmpdir)
	cmds := strings.Split(cmd, " ")
	if _, _, err := run.Simple(nil, nil, cmd); err != nil {
		return fmt.Errorf("failed to run %s: %s", cmd, err)
	}

	src := path.Join(teacherdir, project.AbstractName(), project.Template())
	i, err := os.Stat(src)
	if err != nil {
		return errors.New("could not find template " + project.Template())
	}

	if i.IsDir() {
		root := src

		if err := filepath.Walk(root, func(src string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if src == root {
				/* Do not copy "root" directory. */
				return nil
			}

			/* Elevate destination one node in path (remove "root"). */
			moddst := path.Join(tmpdir, strings.TrimPrefix(src, root))
			return duplicate(moddst, src, uid, gid)
		}); err != nil {
			return err
		}
	} else {
		dst = strings.TrimPrefix(project.Template(), "template-")

		if project.Lang() != "None" {
			dst = strings.TrimSuffix(dst, project.Lang() + proj.LangToExt(project.Lang())) + proj.LangToExt(project.Lang())
		}

		if err := duplicate(path.Join(tmpdir, dst), src, uid, gid); err != nil {
			return fmt.Errorf("failed to duplicate %s to %s", src, dst)
		}
	}

	cmd = fmt.Sprintf("git -C %s add %s", tmpdir, path.Base(dst))
	if _, _, err := run.Simple(nil, nil, cmd); err != nil {
		return fmt.Errorf("failed to run %s: %s", cmd, err)
	}

	cmd = fmt.Sprintf(`git -C %s -c "user.name=%s" -c user.email=%s commit -m "%s"`,
			   tmpdir,
			  "Aquinas administrator",
			   conf.EmailSender(),
			  "Add template")
	if _, _, err := run.Simple(nil, nil, cmd); err != nil {
		return fmt.Errorf("failed to run %s: %s", cmd, err)
	}

	cmd = fmt.Sprintf("git -C %s push", tmpdir)
	if _, _, err := run.Simple(nil, nil, cmd); err != nil {
		return fmt.Errorf("failed to run %s: %s", strings.Join(cmds, " "), err)
	}

	return nil
}

func updateKnownHosts(homedir string, uid, gid int) error {
	cmd := fmt.Sprintf(
		"sudo -u teacher ssh root@%s.%s dropbearkey -y -f /etc/dropbear/dropbear_rsa_host_key",
		conf.HostUser(),
		conf.Domain())
	output, _, err := run.Simple(nil, nil, cmd)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to run %s: %s\n", cmd, err)
		os.Exit(1)
	}

	key := string(output)
	fields := strings.Split(key, "\n")
	if len(fields) != 4 {
		return errors.New("unexpected key material: " + key)
	}
	key = fields[1]

	knownHosts := path.Join(homedir, ".ssh", "known_hosts")
	if err := ioutil.WriteFile(knownHosts, []byte(key), 0600); err != nil {
		return fmt.Errorf("could not store host key: %s", err)
	}

	if err := os.Chown(knownHosts, uid, gid); err != nil {
		return fmt.Errorf("unable set ownership of %s: %s", knownHosts, err)
	}

	return nil
}

func sethooks(projdir string) error {
	hookdir := path.Join(projdir, "hooks")

	i, err := os.Stat(hookdir)
	if err != nil {
		return fmt.Errorf("could not stat existing Git hook directory: %s", err)
	}

	if err := os.RemoveAll(hookdir); err != nil {
		return fmt.Errorf("could not remove existing Git hooks: %s", err)
	}

	if err := os.Mkdir(hookdir, i.Mode()); err != nil {
		return fmt.Errorf("unable to create %s: %s", hookdir, err)
	}

	hook := path.Join(hookdir, "post-receive")
	if err := os.Symlink("/usr/bin/aquinas-git-post-receive", hook); err != nil {
		return fmt.Errorf("unable to create %s: %s", hook, err)
	}

	hook = path.Join(hookdir, "update")
	if err := os.Symlink("/usr/bin/aquinas-git-update", hook); err != nil {
		return fmt.Errorf("unable to create %s: %s", hook, err)
	}

	return nil
}
