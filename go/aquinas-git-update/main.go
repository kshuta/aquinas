package main

import (
	"aquinas/common"
	"fmt"
	"log/syslog"
	"os"
	"path"
	"syscall"
	"github.com/google/uuid"
)

func main() {
	logger, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		panic(err)
	}

	pwd, err := os.Getwd()
	if err != nil {
		common.Fail(logger.Err, err)
	}

	student := path.Base(path.Dir(pwd))
	project := path.Base(pwd)

	logger.Debug(fmt.Sprintf("hooking push of %s by %s", project, student))

	/*
	 * Record that grading started. Grader handles clean up. Do this here,
	 * because job might sit in queue for some time.
	 */
	id := uuid.New()
	dir := path.Join("/tmp", student, "grading")
	file := path.Join(dir, project)
	mask := syscall.Umask(0)
	os.MkdirAll(dir, 0777)
	syscall.Umask(mask)

	flag, err := os.OpenFile(file, os.O_CREATE | os.O_EXCL | os.O_WRONLY, 0644)
	if err != nil {
		if os.IsExist(err) {
			common.Fail(logger.Err, fmt.Errorf("error enqueueing job: grading of %s for %s already underway", project, student))
		} else {
			common.Fail(logger.Err, fmt.Errorf("error enqueueing job: open of %s failed: %s", file, err))
		}
	}

	if _, err := flag.Write([]byte(id.String())); err != nil {
		common.Fail(logger.Err, fmt.Errorf("error enqueueing job: write to %s failed: %s", file, err))
	}

	// Note: post-receive hook enqueues job after new commit available.
}
