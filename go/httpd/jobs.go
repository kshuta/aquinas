package main

import (
	"sync"
)

type jobMap struct {
	jobs map[string]chan struct{}
	lock sync.Mutex
}

func newJobs() *jobMap {
	return &jobMap{jobs: map[string]chan struct{}{}}
}

func (m *jobMap) Lock() {
	m.lock.Lock()
}

func (m *jobMap) Unlock() {
	m.lock.Unlock()
}

func (m *jobMap) UnlockAndRegister(id string) {
	c := make(chan struct{}, 0)
	m.jobs[id] = c
	m.lock.Unlock()
}

func (m *jobMap) Wait(id string) {
	c, ok := m.jobs[id]
	if !ok {
		return
	}

	<-c
}

func (m *jobMap) Notify(id string) bool {
	m.lock.Lock()
	defer m.lock.Unlock()

	c, ok := m.jobs[id]
	if !ok {
		return false
	}
	delete(m.jobs, id)

	select {
	case c <- struct{}{}:
	default:
	}

	return true
}
