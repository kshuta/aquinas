package main

import (
	"aquinas/common"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/user"
	"path"
)

func main() {
	var repo common.GitRepository

	if len(os.Args) != 4 {
		fmt.Fprintf(os.Stderr, "usage: %s USER PROJECT GIT-JSON\n", os.Args[0])
		os.Exit(1)
	}

	student := os.Args[1]
	project := os.Args[2]
	norm    := common.NormalizeUsername(student)

	if err := json.Unmarshal([]byte(os.Args[3]), &repo); err != nil {
		fmt.Fprintf(os.Stderr, "could not unmarshal %s: %s", os.Args[3], err)
		os.Exit(1)
	}

	switch repo.Provider {
	case "gitlab":
		url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s%%2F%s?private_token=%s", repo.Path, project, repo.Token)

		resp, err := http.Get(url)
		if err != nil {
			fmt.Fprintf(os.Stderr, "could not get %s: %s\n", url, err)
			os.Exit(1)
		}

		if resp.StatusCode != http.StatusOK {
			fmt.Printf("false\n")
		} else {
			fmt.Printf("true\n")
		}
	default:
		u, err := user.Lookup(norm)
		if err != nil {
			fmt.Fprintf(os.Stderr, "could not lookup user %s: %s\n", norm, err)
			os.Exit(1)
		}

		if _, err := os.Stat(path.Join(u.HomeDir, project)); err != nil {
			fmt.Printf("false\n")
		} else {
			fmt.Printf("true\n")
		}
	}
}
