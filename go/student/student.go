package student

import (
	"flyn.org/git/safe.git"
)

// Student represents the students who use Aquinas.
type Student struct {
	Password    *safe.String `json:"password"`
	Alias       *safe.String `json:"alias"`
	Key         *safe.Key    `json:"keys"`
	GitProvider *safe.String `json:"git-provider"`
	GitPath     *safe.String `json:"git-path"`
	GitUsername *safe.String `json:"git-username"`
	GitToken    *safe.String `json:"git-token"`
}
