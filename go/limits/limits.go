package limits

import (
	"golang.org/x/sys/unix"
)

// Units.
const (
	KiB = 1024       // Kilobyte.
	MiB = 1024 * KiB // Megabyte.
	GiB = 1024 * MiB // Gigabyte.
)

// Limits specifies the resource limits enforced by Aquinas.
type Limits struct {
	MaxFiles uint64
	MaxThreads uint64
	MaxMsgQueue uint64
}

// Set sets resource limits.
func Set(l Limits) (err error) {
	err = unix.Setrlimit(unix.RLIMIT_CORE,
	                    &unix.Rlimit{0, 0})
	if err != nil {
		return
	}

	err = unix.Setrlimit(unix.RLIMIT_NOFILE,
	                    &unix.Rlimit{l.MaxFiles, l.MaxFiles})
	if err != nil {
		return
	}

	err = unix.Setrlimit(unix.RLIMIT_NPROC,
	                    &unix.Rlimit{l.MaxThreads, l.MaxThreads})
	if err != nil {
		return
	}

	err = unix.Setrlimit(unix.RLIMIT_MSGQUEUE,
	                    &unix.Rlimit{l.MaxMsgQueue, l.MaxMsgQueue})
	if err != nil {
		return
	}

	return
}
