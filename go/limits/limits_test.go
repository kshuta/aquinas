package limits

import (
	"os"
	"os/exec"
	"testing"
	"golang.org/x/sys/unix"
)

func TestConstants(t *testing.T) {
	if KiB != 1024 {
		t.Error("KiB mis-defined")
	}

	if MiB != 1024 * 1024 {
		t.Error("MiB mis-defined")
	}

	if GiB != 1024 * 1024 * 1024 {
		t.Error("GiB mis-defined")
	}
}

func TestLimits(t *testing.T) {
	var l1 unix.Rlimit
	var l2 Limits

	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	err := unix.Getrlimit(unix.RLIMIT_NPROC, &l1)
	if err != nil {
		t.Error(err.Error())
	}
	l2.MaxThreads = l1.Max

	err = unix.Getrlimit(unix.RLIMIT_NOFILE, &l1)
	if err != nil {
		t.Error(err.Error())
	}
	l2.MaxFiles = l1.Max

	err = unix.Getrlimit(unix.RLIMIT_MSGQUEUE, &l1)
	if err != nil {
		t.Error(err.Error())
	}
	l2.MaxMsgQueue = l1.Max

	err = Set(l2)
	if err != nil {
		t.Error(err.Error())
	}

	err = exec.Command("true").Run()
	if err != nil {
		t.Error(err.Error())
	}
}

func TestLimitsThreads(t *testing.T) {
	var l1 unix.Rlimit
	var l2 Limits

	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	l2.MaxThreads = 0

	err := unix.Getrlimit(unix.RLIMIT_NOFILE, &l1)
	if err != nil {
		t.Error(err.Error())
	}
	l2.MaxFiles = l1.Max

	err = unix.Getrlimit(unix.RLIMIT_MSGQUEUE, &l1)
	if err != nil {
		t.Error(err.Error())
	}
	l2.MaxMsgQueue = l1.Max

	err = Set(l2)
	if err != nil {
		t.Error(err.Error())
	}

	err = exec.Command("true").Run()
	if e, ok := err.(*os.PathError); !ok || e.Err.(unix.Errno) != unix.EAGAIN {
		t.Error(err.Error())
	}
}

func TestLimitsFiles(t *testing.T) {
	var l1 unix.Rlimit
	var l2 Limits

	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	err := unix.Getrlimit(unix.RLIMIT_NPROC, &l1)
	if err != nil {
		t.Error(err.Error())
	}
	l2.MaxThreads = l1.Max

	l2.MaxFiles = 3

	err = unix.Getrlimit(unix.RLIMIT_MSGQUEUE, &l1)
	if err != nil {
		t.Error(err.Error())
	}
	l2.MaxMsgQueue = l1.Max

	err = Set(l2)
	if err != nil {
		t.Error(err.Error())
	}

	f, err := os.Open("/dev/null")
	if e, ok := err.(*os.PathError); !ok || e.Err.(unix.Errno) != unix.EMFILE {
		t.Error(err.Error())
	}

	f.Close()
}
