package kahn

import (
	"fmt"
	"testing"
)

func TestKahn(t *testing.T) {
	graph := Node{
		"four": map[string]bool{"three": true},
		"one": nil,
		"three": map[string]bool{"two": true},
		"two": map[string]bool{"one": true},
	}

	sorted1      := []string{ "one", "two", "three", "four" }
	sorted2, err := Kahn(graph)

	if err != nil {
		t.Error(err.Error())
	}

	if len(sorted1) != len(sorted2) {
		t.Error("sort failed:" + fmt.Sprintf("%v", sorted2))
	}

	for i, v := range sorted2 {
		if v != sorted1[i] {
			t.Error("sort failed: " + fmt.Sprintf("%v", sorted2))
		}
	}
}

func TestKahnPartial(t *testing.T) {
	graph := Node{
		"four": map[string]bool{"one": true},
		"three": nil,
		"one": nil,
		"two": nil,
	}

	sorted, err := Kahn(graph)

	if err != nil {
		t.Error(err.Error())
	}

	oneSeen := false
	for _, v := range sorted {
		switch(v) {
		case "four":
			if !oneSeen {
				t.Error("sort failed: four before one")
			}
		case "one":
			oneSeen = true
		}
	}
}

func TestKahnCycle(t *testing.T) {
	graph := Node{
		"one": map[string]bool{"two": true},
		"two": map[string]bool{"one": true},
	}

	_, err := Kahn(graph)
	if err == nil {
		t.Error("expected error but did not receive one")
	}
}
