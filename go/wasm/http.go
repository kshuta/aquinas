package wasm

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"aquinas/proj"
	"aquinas/student"
	"github.com/google/uuid"
	httpx "flyn.org/git/wasm.git/http"
)

var extraParams string

// SetExtraParams sets a query parameter string that will be appended to all HTTP requests.
func SetExtraParams(p string) {
	extraParams = p
}

func getStudent(student, k string) (v string, err error) {
	record := map[string]string{}

	url := fmt.Sprintf("/api/students/%s", student)

	if extraParams != "" {
		url += "?" + extraParams
	}

	if err := httpx.GetAndUnmarshal(url, &record); err != nil {
		return "", err
	}

	return record[k], nil
}

// GetAlias returns the student's alias.
func GetAlias(student string) (alias string, err error) {
	return getStudent(student, "alias")
}

// GetKeys returns the student's SSH keys.
func GetKeys(student string) (keys string, err error) {
	return getStudent(student, "keys")
}

// GetGitProvider returns information that describes the student's Git provider.
func GetGitProvider(student string) (gitProvider, gitPath, gitUsername, gitToken string, err error) {
	record := map[string]string{}

	url := fmt.Sprintf("/api/students/%s", student)

	if extraParams != "" {
		url += "?" + extraParams
	}

	if err := httpx.GetAndUnmarshal(url, &record); err != nil {
		return "", "", "", "", err
	}

	return record["git-provider"], record["git-path"], record["git-username"], record["git-token"], nil
}

// GetCourses returns the list of courses in which the current student is enrolled.
func GetCourses() (courses []string, err error) {
	url := "/api/courses"

	if extraParams != "" {
		url += "?" + extraParams
	}

	err = httpx.GetAndUnmarshal(url, &courses)
	return
}

// GetRanks returns a mapping of projects complete to students.
func GetRanks() (ranks map[int][]string, err error) {
	url := "/api/stats-ranks"

	if extraParams != "" {
		url += "?" + extraParams
	}

	err = httpx.GetAndUnmarshal(url, &ranks)
	return
}

// Get returns a mapping of projects complete to students.
func GetStatsCourse(course string) (grades map[string][]proj.Grade, err error) {
	url := fmt.Sprintf("/api/stats-courses/%s", course)

	if extraParams != "" {
		url += "?" + extraParams
	}

	err = httpx.GetAndUnmarshal(url, &grades)
	return
}

// GetGradesBrief returns a summary of all grades.
func GetGradesBrief() (grades []proj.Grade, err error) {
	url := "/api/stats-grades?brief=true"

	if extraParams != "" {
		url += "&" + extraParams
	}

	err = httpx.GetAndUnmarshal(url, &grades)
	return
}

// GetGradesStudentBrief returns a summary of the student's grades.
func GetGradesStudentBrief(student string) (grades []proj.Grade, err error) {
	url := fmt.Sprintf("/api/stats-grades?student=%s&brief=true", student)

	if extraParams != "" {
		url += "&" + extraParams
	}

	err = httpx.GetAndUnmarshal(url, &grades)
	return
}

// GetGradesStudentProject returns a student's grades for the given project.
func GetGradesStudentProject(student, project string) (grades []proj.Grade, err error) {
	url := fmt.Sprintf("/api/stats-grades?student=%s&project=%s", student, project)

	if extraParams != "" {
		url += "&" + extraParams
	}

	err = httpx.GetAndUnmarshal(url, &grades)
	return
}

// GetGradesNow triggers a grading sequence and returns the project's grades
// when it finishes.
func GetGradesNow(project string) (grades []proj.Grade, err error) {
	url := fmt.Sprintf("/api/grades-now?project=%s", project)

	if extraParams != "" {
		url += "&" + extraParams
	}

	err = httpx.GetAndUnmarshal(url, &grades)
	return
}

// GetIsGrading returns a UUID that corresponds to a grading job; the UUID
// will contain all zeros if Aquinas is not presently grading the project.
func GetIsGrading(project string) (jobUUID uuid.UUID, err error) {
	var s string
	url := fmt.Sprintf("/api/is-grading?project=%s", project)

	if extraParams != "" {
		url += "&" + extraParams
	}

	if err = httpx.GetAndUnmarshal(url, &s); err != nil {
		return
	}

	jobUUID, err = uuid.Parse(s)

	return
}

// GetProjects returns the list of projects.
func GetProjects() (projectList map[string]proj.Project, err error) {
	url := "/api/projects"

	if extraParams != "" {
		url += "?" + extraParams
	}

	err = httpx.GetAndUnmarshal(url, &projectList)
	return
}

// GetProjectsNamed returns the instructions associated with the given project.
func GetProjectsNamed(project string) (inst proj.Instructions, err error) {
	url := fmt.Sprintf("/api/project/%s", project)

	if extraParams != "" {
		url += "?" + extraParams
	}

	err = httpx.GetAndUnmarshal(url, &inst)
	return
}

func getRegisterReset(op, student, params string) (err error) {
	ok := false
	url := fmt.Sprintf("/api/%s/%s?%s", op, student, params)

	if extraParams != "" {
		url += "&" + extraParams
	}

	err = httpx.GetAndUnmarshal(url, &ok)
	if err == nil && !ok {
		err = errors.New("received unexpected response")
	}

	return err
}

// GetRegister returns the information needed to register.
func GetRegister(student, params string) error {
	url := "register"

	if extraParams != "" {
		url += "?" + extraParams
	}

	return getRegisterReset(url, student, params)
}

// GetResetPassword returns the information needed to reset the student's
// password.
func GetResetPassword(student, params string) error {
	url := "reset-password"

	if extraParams != "" {
		url += "?" + extraParams
	}

	return getRegisterReset(url, student, params)
}

// GetWait blocks until the job with the given ID completes.
func GetWait(id string) (err error) {
	ok := false
	url := fmt.Sprintf("/api/wait?id=%s", id)

	if extraParams != "" {
		url += "&" + extraParams
	}

	err = httpx.GetAndUnmarshal(url, &ok)
	if err == nil && !ok {
		err = errors.New("received unexpected response")
	}

	return err
}

// PostStudent sets the information associated with a student.
func PostStudent(name string, s student.Student) (err error) {
	j, err := json.Marshal(s)
	if err != nil {
		return
	}

	values := url.Values{"description": []string{string(j)}}

	ok := false
	url := fmt.Sprintf("/api/students/%s", name)

	if extraParams != "" {
		url += "?" + extraParams
	}

	err = httpx.PostAndUnmarshal(url, values, &ok)
	if err == nil && !ok {
		err = errors.New("received unexpected response")
	}

	return err
}

// PostRegister completes a student registration.
func PostRegister(student string, values url.Values) (id string, err error) {
	url := fmt.Sprintf("/api/register/%s", student)

	if extraParams != "" {
		url += "?" + extraParams
	}

	err = httpx.PostAndUnmarshal(url, values, &id)
	return id, err
}

// PostResetPassword completes a student password reset.
func PostResetPassword(student string, values url.Values) (err error) {
	ok := false
	url := fmt.Sprintf("/api/reset-password/%s", student)

	if extraParams != "" {
		url += "?" + extraParams
	}

	err = httpx.PostAndUnmarshal(url, values, &ok)
	if err == nil && !ok {
		err = errors.New("received unexpected response")
	}
	return err
}

// PostFork forks the canonical repository for given project.
func PostFork(project string) (id string, err error) {
	url := fmt.Sprintf("/api/project/%s/fork", project)

	if extraParams != "" {
		url += "?" + extraParams
	}

	err = httpx.PostAndUnmarshal(url, nil, &id)
	return id, err
}

// DeleteStudentsNamed deletes the given student.
func DeleteStudentsNamed(student string) error {
	url := fmt.Sprintf("/api/students/%s", student)

	if extraParams != "" {
		url += "?" + extraParams
	}

	resp, err := httpx.Delete(url)
	if err != nil {
                return fmt.Errorf("%v: %v", url, err)
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
                return fmt.Errorf("%v: %v", url, err)
	}

	if resp.StatusCode != http.StatusOK {
                return fmt.Errorf("%v: bad status code: %v", url, resp.StatusCode)
	}

	ok := false
	if err := json.Unmarshal(data, &ok); err != nil {
                return fmt.Errorf("%v: %v", url, err)
	}

	if !ok {
                return fmt.Errorf("%v: received unexpected response")
	}

	return nil
}
