package main

import (
	"aquinas/db"
	"fmt"
	"os"
)

func main() {
	var student, password string

	if len(os.Args) != 3 {
		fmt.Fprintf(os.Stderr, "usage: %s USER PASSWORD\n", os.Args[0])
		os.Exit(1)
	}

	student  = os.Args[1]
	password = os.Args[2]

	db := make(db.Filesystem)

	if err := db.SetPassword(student, password); err != nil {
		fmt.Fprintf(os.Stderr, "could not reset student's password: %s\n", err)
		os.Exit(1)
	}
}
