package main

import (
	"aquinas/common"
	"fmt"
	"aquinas/queue"
	"log/syslog"
	"os"
	"path"
)

func main() {
	logger, err := syslog.New(syslog.LOG_NOTICE, os.Args[0])
	if err != nil {
		panic(err)
	}

	pwd, err := os.Getwd()
	if err != nil {
		common.Fail(logger.Err, err)
	}

	student := path.Base(path.Dir(pwd))
	project := path.Base(pwd)

	logger.Debug(fmt.Sprintf("hooking push of %s by %s", project, student))

	// Note: update hook records that grading started. See comments there.

	// Equeue grade job.
	stdout, stderr, err := queue.Enqueue("/usr/sbin/grader", student, project)
	if err != nil {
		common.Fail(logger.Err, fmt.Errorf("error enqueueing job: %s", err))
	}

	fmt.Fprintf(os.Stdout, "%s", stdout)
	fmt.Fprintf(os.Stderr, "%s", stderr)
}
