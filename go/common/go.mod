module common

go 1.16

replace aquinas/conf => ../conf

replace aquinas/run => ../run

require (
	aquinas/conf v0.0.0-00010101000000-000000000000 // indirect
	aquinas/run v0.0.0-00010101000000-000000000000 // indirect
)
