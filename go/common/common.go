package common

import (
	"aquinas/conf"
	"crypto/rand"
	"crypto/sha256"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"os"
	"path"
	"reflect"
	"regexp"
	"aquinas/run"
	"strings"
	"time"
)

const (
	passwordLen = 16
)

// A GitRepository contains information describing the Git
// repository that hosts a student's submissions.
type GitRepository struct {
	Provider string `json:"git-provider"`
	Path     string `json:"git-path"`
	Username string `json:"git-username"`
	Token    string `json:"git-token"`
}

// Fail writes to Log, prints to stderr, and terminates execution.
func Fail(fn func (message string) error, err error) {
	/*
	 * Printing to stderr is sometimes necessary; for example, when
	 * queue calls grader in response to HTTP api/grades-now or when
	 * pulling error messages from buildrunsh.
	 */
	fmt.Fprintf(os.Stderr, "%s\n", err)

	/* Print to specified logger. */
	fn(err.Error())

	os.Exit(1)
}

func execWithStderr(arg0 string, args ...string) (err error) {
	_, _, err = run.Timed(5 * time.Second, nil, nil, arg0, args...)

	return
}

// UpdateRepo clones or pulls the given Git repository and branch.
func UpdateRepo(repo, branch, dir, projName string) (err error) {
	repoDir := path.Join(dir, projName)

	stat, err := os.Stat(repoDir)
	if os.IsNotExist(err) {
		if err = execWithStderr("git", "clone", "-q", repo, "-b", branch, repoDir);
		   err != nil {
			return
		}
	} else if stat.IsDir() {
		if err = execWithStderr("git", "-C", repoDir, "pull");
		   err != nil {
			return
		}
	} else {
		return errors.New(repoDir + " exists but is not a directory")
	}

	return
}

// Nonce returns a 16-byte nonce.
func Nonce() string {
	return fmt.Sprintf("%X", time.Now().UnixNano())
}

// Password returns a random string suitable for use as a password.
func Password() (string, error) {
	b := make([]byte, passwordLen / 2) // before hexify, so half length.

	if _, err := rand.Read(b); err != nil {
		return "", err
	}

	return fmt.Sprintf("%X", b), nil
}

// HashCalc computes the hash of the given data, returning it as a string.
func HashCalc(data string) string {
	return fmt.Sprintf("%X", sha256.Sum256([]byte(data)))
}

// HashSame checks to see if two hash strings are the same, ignoring
// differences in case.
func HashSame(h1, h2 string) bool {
	return strings.ToUpper(h1) == strings.ToUpper(h2)
}

// NormalizeUsername transforms an email address into something that can serve
// as a username.
func NormalizeUsername(username string) string {
	username = strings.Replace(username, "@", "_at_", -1)
	username = strings.Replace(username, ".", "_dot_", -1)

	if len(username) > 32 {
		sum := md5.Sum([]byte(username))
		username = hex.EncodeToString(sum[:])
		username = "u" + username[1:]
	}

	return username
}

// HasNil checks a structure for fields that are nil (ignores field named ignore).
func HasNil(x interface{}, ignore string) bool {
	rv := reflect.ValueOf(x)
	rv  = rv.Elem()

	for i := 0; i < rv.NumField(); i++ {
		if rv.Type().Field(i).Name != ignore && rv.Field(i).IsNil() {
			return true
		}
	}

	return false
}

// AllowedEmailDomain checks whether an email address is permitted according to
// Aquinas' configuration.
func AllowedEmailDomain(email string) bool {
	/* Allow example.com permits testing; see test-case-battery-07-www-register. */
	allowed := regexp.MustCompile(conf.EmailRegex())

	return allowed.Match([]byte(email))
}

// SplitBaseLang takes a project name and returns the project's abstract name
// and language.
func SplitBaseLang(projName string) (projBase, projLang string) {
	amd64     := "AMD64"
	amd64Exp  := regexp.MustCompile(`.*` + amd64)
	bourne    := "Bourne"
	bourneExp := regexp.MustCompile(`.*` + bourne)
	c         := "C"
	cExp      := regexp.MustCompile(`.*` + c)
	golang    := "Go"
	goExp     := regexp.MustCompile(`.*` + golang)
	java      := "Java"
	javaExp   := regexp.MustCompile(`.*` + java)
	python    := "Python"
	pythonExp := regexp.MustCompile(`.*` + python)

	switch {
	case amd64Exp.MatchString(projName):
		projLang = amd64
		projBase = strings.TrimSuffix(projName, projLang)
	case bourneExp.MatchString(projName):
		projLang = bourne
		projBase = strings.TrimSuffix(projName, projLang)
	case cExp.MatchString(projName):
		projLang = c
		projBase = strings.TrimSuffix(projName, projLang)
	case goExp.MatchString(projName):
		projLang = golang
		projBase = strings.TrimSuffix(projName, projLang)
	case javaExp.MatchString(projName):
		projLang = java
		projBase = strings.TrimSuffix(projName, projLang)
	case pythonExp.MatchString(projName):
		projLang = python
		projBase = strings.TrimSuffix(projName, projLang)
	default:
		projLang = ""
		projBase = projName
	}

	return
}

// BuildRepo crafts a Git repository URL.
func BuildRepo(student, project, gitProvider, gitPath, gitUsername, gitToken string) string {
	norm := NormalizeUsername(student)

	switch (gitProvider) {
	case "gitlab":
		return fmt.Sprintf("https://%s:%s@gitlab.com/%s/%s", gitUsername, gitToken, gitPath, project)
	default:
		gitHost := conf.HostGit() + "." + conf.Domain()
		return fmt.Sprintf("%s@%s:%s", norm, gitHost, path.Join(conf.DataRoot(), student, project))
	}
}
