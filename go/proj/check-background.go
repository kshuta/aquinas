package proj

import (
	"bytes"
	"errors"
	"aquinas/run"
	"strings"
)

type checkParametersBackground struct {
	Command   string `json:"command"`
	Stdin   []byte   `json:"stdin"`
}

func runCheckBackground(p ProjectInterface, c Check) (Result, error) {
	var errMsg string

	cmd := strings.Fields(c.Command())
	cmd = substitute(cmd)

	err := run.Background(bytes.NewReader(c.stdin()),
	                      nil,
	                      cmd[0],
	                      cmd[1:]...)
	if err != nil {
		errMsg = err.Error()
	}

	return Result{
		Command:  c.Command(),
		ErrMsg:   errMsg,
	}, nil
}

func (p checkParametersBackground) Grade(i int, result Result) (err error) {
	switch result.ExitCode {
	case 126:
		err = errors.New("could not run program")
	case 127:
		err = errors.New("could not find program")
	}

	return
}

func (p checkParametersBackground) hint() string {
	return ""
}
