package proj

import (
	"bytes"
	"errors"
	"fmt"
	"encoding/json"
	"os"
	"regexp"
	"aquinas/run"
	"strings"
	"time"
)

const maxSubmissionRuntime = 15

func dropQuotes(s string) (s2 []string) {
	r := regexp.MustCompile(`[^\s"']+|"([^"]*)"|'([^']*)'`)
	ss := r.FindAllString(s, -1)

	for i, f := range ss {
		if len(f) > 0 && (f[0] == '"' || f[0] == '\'') {
			ss[i] = f[1:len(f) - 1]
		}
	}

	return ss
}

func runCheck(p ProjectInterface, c Check) (Result, error) {
	var errMsg string
	var checkExitCode int
	var timelimit time.Duration = maxSubmissionRuntime

	cmd := dropQuotes(c.Command())

	cmd, err := p.ApplyRuntimeAltmain(cmd)
	if err != nil {
		os.Stderr.WriteString("system error: buildrun: " + err.Error())
		return Result{}, err
	}

	cmd = substitute(cmd)

	if c.Timelimit != 0 {
		timelimit = c.Timelimit
	}

	stdout, stderr, err := run.Timed(timelimit * time.Second,
	                                 bytes.NewReader(c.stdin()),
	                                 nil,
	                                 cmd[0],
	                                 cmd[1:]...)
	if err != nil {
		if stderrErr, ok := err.(*run.ExitError); ok {
			checkExitCode = stderrErr.ExitCode
			err = nil
		} else {
			errMsg = c.Command() + ": " + err.Error()
		}
	}

	return Result{
		Command:  c.Command(),
		ErrMsg:   errMsg,
		Stdout:   stdout,
		Stderr:   stderr,
		ExitCode: checkExitCode,
	}, err
}

// Command upacks the command associated with a grade check.
func (c Check) Command() string {
	switch c.Kind {
	case "basic":
		p := new(checkParameters)
		json.Unmarshal(c.Parameters, p)
		return p.Command
	case "background":
		p := new(checkParametersBackground)
		json.Unmarshal(c.Parameters, p)
		return p.Command
	case "regexp":
		p := new(checkParametersRegexp)
		json.Unmarshal(c.Parameters, p)
		return p.Command
	case "disallowed-regexp":
		p := new(checkParametersDisallowedRegexp)
		json.Unmarshal(c.Parameters, p)
		return p.Command
	}

	return ""
}

func (c Check) stdin() []byte {
	switch c.Kind {
	case "basic":
		p := new(checkParameters)
		json.Unmarshal(c.Parameters, p)
		return p.Stdin
	case "background":
		p := new(checkParametersBackground)
		json.Unmarshal(c.Parameters, p)
		return p.Stdin
	case "regexp":
		p := new(checkParametersRegexp)
		json.Unmarshal(c.Parameters, p)
		return p.Stdin
	case "disallowed-regexp":
		p := new(checkParametersDisallowedRegexp)
		json.Unmarshal(c.Parameters, p)
		return p.Stdin
	}

	return []byte{}
}

// Run executes a grading check.
func (c Check) Run(p ProjectInterface) (Result, error) {
	switch (c.Kind) {
	case "background":
		return runCheckBackground(p, c)
	default:
		return runCheck(p, c)
	}
}

type checkParameters struct {
	Command   string `json:"command"`
	Stdin   []byte   `json:"stdin"`
	Stdout  []byte   `json:"stdout"`
	Stderr  []byte   `json:"stderr"`
	ExitCode  int    `json:"exitCode"`
	Hint      string `json:"hint"`
}

// Grade checks the result of the execution of a check
func (p checkParameters) Grade(i int, result Result) (err error) {
	if result.ErrMsg != "" {
		return fmt.Errorf("%s", result.ErrMsg)
	}

	if !bytes.Equal(result.Stdout, p.Stdout) {
		s1  := truncate(result.Stdout)
		s2  := truncate(p.Stdout)
		msg := fmt.Sprintf("stdout check %d failed: got %s wanted %s",
				    i, s1, s2)
		err  = errors.New(msg)
		return
	}

	if !bytes.Equal(result.Stderr, p.Stderr) {
		s1  := truncate(result.Stderr)
		s2  := truncate(p.Stdout)
		msg := fmt.Sprintf("stderr check %d failed: got %s wanted %s",
				   i, s1, s2)
		err  = errors.New(msg)
		return
	}

	if result.ExitCode != p.ExitCode {
		e1  := result.ExitCode
		e2  := p.ExitCode
		msg := fmt.Sprintf("exit code check %d failed: got %d wanted %d",
				    i, e1, e2)
		err  = errors.New(msg)
		return
	}

	return
}

func (p checkParameters) hint() string {
	return p.Hint
}

// Grade checks a series of grade check results.
func (c Checks) Grade(results Results) (string, error) {
	if len(results.Results) != len(c) {
		err := fmt.Errorf("results count is %d, not %d",
		                   len(results.Results),
		                   len(c))
		return "system error", err
	}

	for i, check := range c {
		var p interface{
			Grade(int, Result) error
			hint() string
		}

		switch check.Kind {
		case "basic":
			p = new(checkParameters)
		case "background":
			p = new(checkParametersBackground)
		case "regexp":
			p = new(checkParametersRegexp)
		case "disallowed-regexp":
			p = new(checkParametersDisallowedRegexp)
		default:
			err := errors.New("unknown check type " + check.Kind)
			return "system error", err
		}

		if err := json.Unmarshal(check.Parameters, p); err != nil {
			return "system error", err
		}

		err := p.Grade(i, results.Results[i])
		if err != nil {
			s := fmt.Sprintf("Check %d/%d failed.", i + 1, len(c))

			if p.hint() != "" {
				s += " " + p.hint()
			}

			return s, err
		}
	}

	return "", nil
}

func truncate(b []byte) string {
	if len(b) > 64 {
		return strings.ReplaceAll(string(b[:64]), "\n", "[nl]")
	}

	return strings.ReplaceAll(string(b), "\n", "[nl]")
}
