package proj

import (
	"strings"
	"testing"
)

func TestRegexpGrade(t *testing.T) {
	p := checkParametersRegexp{
		Command: "test",
		Stdin:  []byte{},
		Stdout: []string{ "(?s)test.*test" },
		Stderr: []string{},
		ExitCode: 0,
	}

	r := Result{
		Command: "test",
		ErrMsg:  "",
		Stdout: []byte("test\ntest"),
		Stderr: []byte{},
		ExitCode: 0,
	}

	err := p.Grade(0, r)
	if err != nil {
		t.Error("check failed " + err.Error())
	}
}

func TestDecodeProject(t *testing.T) {
	data := `{ "name": "test",
	           "languages": [ "C", "Go" ],
	           "prerequisites": [ "parent" ],
	           "forbidden": "goto",
	           "compiler": "C",
	           "altmain": { "C": true },
	           "checks": [{ "kind": "basic",
		                "parameters": {
					"command": "true",
					"stdin": null,
					"stdout": null,
					"stderr": null,
					"exitCode": 0
				}
	                     }]
	         }`

	reader := strings.NewReader(data)
	p, err := DecodeAndInstantiateProject(reader, "C")
	if err != nil {
		t.Error(err.Error())
	}

	if p.AbstractName() != "test" {
		t.Error("did not properly decode abstract name from " + data)
	}

	if p.Name() != "testC" {
		t.Error("did not properly decode name from " + data)
	}

	if p.Lang() != "C" {
		t.Error("did not properly decode language from " + data)
	}

	if len(p.AbstractPrerequisites()) != 1 || p.AbstractPrerequisites()[0] != "parent" {
		t.Error("did not properly decode prerequisites from " + data)
	}

	if p.Forbidden() != "goto" {
		t.Error("did not properly decode forbidden from " + data)
	}

	if p.Altmain() != "main2.c" {
		t.Error("did not properly decode altmain from " + data)
	}

	if p.Checks()[0].Command() != "true" {
		t.Error("did not properly decode command from " + data)
	}
}
