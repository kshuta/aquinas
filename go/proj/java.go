package proj

import (
	"fmt"
	"os"
	"aquinas/run"
	"strings"
)

type projectJava PrivateProject

func newProjectJava(ap AbstractProject) (p *projectJava) {
	p = new(projectJava)

	p.NameF = ap.Name + "Java"
	p.AbstractNameF = ap.Name
	p.LanguageF = "Java"
	p.TagsF = ap.Tags
	p.AbstractPrerequisitesF = ap.Prerequisites
	p.SummaryF = ap.Summary

	if len(ap.Checks) != 0 {
		p.GradedF = true
	}

	if altmain, ok := ap.Altmain["Java"]; ok {
		p.AltmainF = altmain
	}

	p.ForbiddenF = ap.Forbidden
	p.ChecksF = ap.Checks
	p.ServicesF = ap.Services
	p.FilesF = ap.Files
	p.ServiceFilesF = ap.ServiceFiles
	p.ServiceLinksF = ap.ServiceLinks
	p.TemplateF = ap.Templates["Java"]

	return
}

func (proj projectJava) Name() string { return proj.NameF }
func (proj projectJava) AbstractName() string { return proj.AbstractNameF }
func (proj projectJava) Summary() string { return proj.SummaryF }
func (proj projectJava) AbstractPrerequisites() []string { return proj.AbstractPrerequisitesF }
func (proj projectJava) Forbidden() string { return proj.ForbiddenF }
func (proj projectJava) Checks() Checks { return proj.ChecksF }
func (proj projectJava) Services() []service { return proj.ServicesF }
func (proj projectJava) Files() []string { return proj.FilesF }
func (proj projectJava) Lang() string { return "Java" }
func (proj projectJava) Template() string { return proj.TemplateF }

func (proj projectJava) Altmain() string {
	if proj.AltmainF {
		return "Main2" + LangToExt(proj.Lang())
	}

	return ""
}

func (proj projectJava) SrcFile() string {
	capName := strings.Title(proj.AbstractNameF)
	return capName + LangToExt(proj.Lang())
}

func (proj projectJava) Build() (err error) {
	srcFile := proj.SrcFile()
	args := []string{"-cp", "/usr/lib/junit.jar", srcFile}

	if altmain := proj.Altmain(); altmain != "" {
		args = append(args, altmain)
	}

	if _, _, err = run.Standard(nil, nil, "javac", args...); err != nil {
		return fmt.Errorf("failed building %s: %s", proj.Name(), err)
	}

	f, err := os.OpenFile(proj.AbstractNameF, os.O_WRONLY | os.O_CREATE, 0755)
	if err != nil {
		return err
	}

	defer f.Close()

	/* Create driver program, as checks often expect ./$projname. */
	if proj.Altmain() == "" {
		fmt.Fprintf(f, "#!/bin/sh\n\njava %s $*\n", strings.Title(proj.AbstractNameF))
	} else {
		fmt.Fprintf(f, "#!/bin/sh\n\njava Main2 $*\n")
	}

	return
}

func (proj projectJava) ApplyRuntimeAltmain(cmd []string) (cmd2 []string, err error) { return cmd, err }
