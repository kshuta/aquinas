package proj

import (
	"errors"
	"fmt"
	"regexp"
)

type checkParametersDisallowedRegexp struct {
	Command   string `json:"command"`
	Stdin   []byte   `json:"stdin"`
	Stdout  []string `json:"stdout"`
	Stderr  []string `json:"stderr"`
	ExitCode  int    `json:"exitCode"`
	Hint      string `json:"hint"`
}

func (p checkParametersDisallowedRegexp) Grade(i int, result Result) (err error) {
	if result.ErrMsg != "" {
		return fmt.Errorf("%s", result.ErrMsg)
	}

	for j, s := range p.Stdout {
		var exp *regexp.Regexp

		exp, err = regexp.Compile(s)
		if err != nil {
			return
		}

		if exp.MatchString(string(result.Stdout)) {
			s1  := truncate(result.Stdout)
			s2  := truncate([]byte(s))
			msg := fmt.Sprintf("stdout check %d.%d failed: got %s did not want regexp %s",
			                    i, j, s1, s2)
			err  = errors.New(msg)
			return
		}
	}

	for j, s := range p.Stderr {
		var exp *regexp.Regexp

		exp, err = regexp.Compile(s)
		if err != nil {
			return
		}

		if exp.MatchString(string(result.Stderr)) {
			s1  := truncate(result.Stderr)
			s2  := truncate([]byte(s))
			msg := fmt.Sprintf("stdout check %d.%d failed: got %s did not want regexp %s",
			                    i, j, s1, s2)
			err  = errors.New(msg)
			return
		}
	}

	if result.ExitCode != p.ExitCode {
		e1  := result.ExitCode
		e2  := p.ExitCode
		msg := fmt.Sprintf("exit code check %d failed: got %d wanted %d",
				    i, e1, e2)
		err  = errors.New(msg)
		return
	}

	return
}
func (p checkParametersDisallowedRegexp) hint() string {
	return p.Hint
}
