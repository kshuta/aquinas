package proj

import (
	"aquinas/conf"
)

func substitute(_args []string) (args []string) {
	for _, arg := range _args {
		switch arg {
		case "TARGET":
			args = append(args, conf.HostTarget() + "." + conf.Domain())
		case "USER":
			args = append(args, conf.HostUser() + "." + conf.Domain())
		default:
			args = append(args, arg)
		}
	}

	return args
}
