package proj

import (
	"aquinas/run"
	"fmt"
)

type projectGo PrivateProject

func newProjectGo(ap AbstractProject) (p *projectGo) {
	p = new(projectGo)

	p.NameF = ap.Name + "Go"
	p.AbstractNameF = ap.Name
	p.LanguageF = "Go"
	p.TagsF = ap.Tags
	p.AbstractPrerequisitesF = ap.Prerequisites
	p.SummaryF = ap.Summary

	if len(ap.Checks) != 0 {
		p.GradedF = true
	}

	if altmain, ok := ap.Altmain["Go"]; ok {
		p.AltmainF = altmain
	}

	p.ForbiddenF = ap.Forbidden
	p.ChecksF = ap.Checks
	p.ServicesF = ap.Services
	p.FilesF = ap.Files
	p.ServiceFilesF = ap.ServiceFiles
	p.ServiceLinksF = ap.ServiceLinks
	p.TemplateF = ap.Templates["Go"]

	return
}

func (proj projectGo) Name() string { return proj.NameF }
func (proj projectGo) AbstractName() string { return proj.AbstractNameF }
func (proj projectGo) Summary() string { return proj.SummaryF }
func (proj projectGo) AbstractPrerequisites() []string { return proj.AbstractPrerequisitesF }
func (proj projectGo) Forbidden() string { return proj.ForbiddenF }
func (proj projectGo) Checks() Checks { return proj.ChecksF }
func (proj projectGo) Services() []service { return proj.ServicesF }
func (proj projectGo) Files() []string { return proj.FilesF }
func (proj projectGo) Lang() string { return "Go" }
func (proj projectGo) Template() string { return proj.TemplateF }

func (proj projectGo) Altmain() string {
	if proj.AltmainF {
		return "main2" + LangToExt(proj.Lang())
	}

	return ""
}

func (proj projectGo) SrcFile() string {
	return proj.AbstractNameF + LangToExt(proj.Lang())
}

func (proj projectGo) Build() (err error) {
	srcFile := proj.SrcFile()
	args := []string{"build", srcFile}

	if altmain := proj.Altmain(); altmain != "" {
		args = append(args, altmain)
	}

	if _, _, err = run.Standard(nil, []string{"CGO_ENABLED=0"}, "go", args...); err != nil {
		return fmt.Errorf("failed building %s: %s", proj.Name(), err)
	}

	return
}

func (proj projectGo) ApplyRuntimeAltmain(cmd []string) (cmd2 []string, err error) { return cmd, err }
