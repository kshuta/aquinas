package proj

import (
	"fmt"
	"os"
	"aquinas/run"
)

type projectC PrivateProject

func newProjectC(ap AbstractProject) (p *projectC) {
	p = new(projectC)

	p.NameF = ap.Name + "C"
	p.AbstractNameF = ap.Name
	p.LanguageF = "C"
	p.TagsF = ap.Tags
	p.AbstractPrerequisitesF = ap.Prerequisites
	p.SummaryF = ap.Summary

	if len(ap.Checks) != 0 {
		p.GradedF = true
	}

	if altmain, ok := ap.Altmain["C"]; ok {
		p.AltmainF = altmain
	}

	p.ForbiddenF = ap.Forbidden
	p.ChecksF = ap.Checks
	p.ServicesF = ap.Services
	p.FilesF = ap.Files
	p.ServiceFilesF = ap.ServiceFiles
	p.ServiceLinksF = ap.ServiceLinks
	p.TemplateF = ap.Templates["C"]

	return
}

func (proj projectC) Name() string { return proj.NameF }
func (proj projectC) AbstractName() string { return proj.AbstractNameF }
func (proj projectC) Summary() string { return proj.SummaryF }
func (proj projectC) AbstractPrerequisites() []string { return proj.AbstractPrerequisitesF }
func (proj projectC) Forbidden() string { return proj.ForbiddenF }
func (proj projectC) Checks() Checks { return proj.ChecksF }
func (proj projectC) Services() []service { return proj.ServicesF }
func (proj projectC) Files() []string { return proj.FilesF }
func (proj projectC) Lang() string { return "C" }
func (proj projectC) Template() string { return proj.TemplateF }

func (proj projectC) Altmain() string {
	if proj.AltmainF {
		return "main2" + LangToExt(proj.Lang())
	}

	return ""
}

func (proj projectC) SrcFile() string {
	return proj.AbstractNameF + LangToExt(proj.Lang())
}

func (proj projectC) Build() (err error) {
	if _, err = os.Stat("Makefile"); !os.IsNotExist(err) {
		if _, _, err = run.Standard(nil, nil, "make"); err != nil {
			return fmt.Errorf("failed building %s: %s", proj.Name(), err)
		}
	} else {
		srcFile := proj.SrcFile()
		exeFile := proj.AbstractNameF
		args := []string{"-o", exeFile}

		if altmain := proj.Altmain(); altmain != "" {
			args = append(args, "-Wl,-emain2", altmain)
		}

		args = append(args, srcFile)

		if _, _, err = run.Standard(nil, nil, "gcc", args...); err != nil {
			return fmt.Errorf("failed building %s: %s", proj.Name(), err)
		}
	}

	return
}

func (proj projectC) ApplyRuntimeAltmain(cmd []string) (cmd2 []string, err error) { return cmd, err }
