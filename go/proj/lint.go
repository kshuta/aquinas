package proj

import (
	"fmt"
	"bufio"
	"os"
	"strings"
)

func lintExecutable(file string) error {
	info, err := os.Stat(file)
	if err != nil {
		return fmt.Errorf("could not read %s: %s", file, err)
	}

	if info.Mode() & 0100 == 0 {
		return fmt.Errorf("%s is not executable", file)
	}

	return nil
}

func lintShebang(file, shebang string) error {
	f, err := os.Open(file)
	if err != nil {
		return fmt.Errorf("could not open %s: %s", file, err)
	}

	defer f.Close()

	shebang2, err := bufio.NewReader(f).ReadString('\n')
	if err != nil {
		return fmt.Errorf("could not read from %s: %s", file, err)
	}
	shebang2 = strings.TrimSpace(shebang2)

	if shebang != shebang2 {
		return fmt.Errorf("%s does not contain shebang (saw \"%s\", not \"%s\")", file, shebang2, shebang)
	}

	return nil
}
