package proj

import (
	"fmt"
	"testing"
)

func TestDropQuotes(t *testing.T) {
	cmd := dropQuotes(`foo "bar" 'baz'`)
	if cmd[0] != "foo" || cmd[1] != "bar" || cmd[2] != "baz" {
		t.Error(fmt.Sprintf("dropQuotes returned %v", cmd))
	}
}
