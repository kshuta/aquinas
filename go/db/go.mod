module db

replace aquinas/common => ../common

replace aquinas/conf => ../conf

replace aquinas/course => ../course

replace aquinas/proj => ../proj

replace aquinas/run => ../run

go 1.16

require (
	aquinas/common v0.0.0-00010101000000-000000000000
	aquinas/conf v0.0.0-00010101000000-000000000000
	aquinas/course v0.0.0-00010101000000-000000000000
	aquinas/proj v0.0.0-00010101000000-000000000000
	aquinas/run v0.0.0-00010101000000-000000000000
	github.com/google/uuid v1.3.0
)
