package db

import (
	"aquinas/course"
	"github.com/google/uuid"
	"aquinas/proj"
)

// Dummy is a dummy database of account, access, and submission information.
type Dummy struct {
	_kv map[string]string
}

// AddStudent creates the given student and assigned him the given password.
func (d *Dummy) AddStudent(user, password string) (jobUUID string, err error) {
	return uuid.New().String(), nil
}

// RemoveStudent removes the given student.
func (d *Dummy) RemoveStudent(user string) (jobUUID string, err error) {
	return uuid.New().String(), nil
}

// Authenticate checks to see if the given password is correct for the given
// user.
func (d *Dummy) Authenticate(user, password string) (err error) {
	return
}

// SetPassword sets the given user's password to the given value.
func (d *Dummy) SetPassword(user, password string) (err error) {
	return
}

// Alias gets the alias for the given user.
func (d *Dummy) Alias(user string) (alias string, err error) {
	return d._kv["alias"], nil
}

// SetAlias sets the given user's password to the given value.
func (d *Dummy) SetAlias(user, alias string) (err error) {
	d._kv["alias"] = alias
	return
}

// KV gets the value corresponding with the given key for the given user.
func (d *Dummy) KV(user, k string) (v string, err error) {
	return d._kv[k], nil
}

// SetKV sets the given user's key to the given value.
func (d *Dummy) SetKV(user, k, v string) (err error) {
	d._kv[k] = v
	return
}

// SSHAuthKeys gets the SSH keys for the given user.
func (d *Dummy) SSHAuthKeys(user string) (key string, err error) {
        return "Fake SSH key", nil
}

// SetSSHAuthKeys sets the SSH keys for the given user to the given value.
func (d *Dummy) SetSSHAuthKeys(user, key string) (jobUUID string, err error) {
	return uuid.New().String(), nil
}

// Last returns the last submission record for the given user and file (project).
func (d *Dummy) Last(root, user, file string) (last string, err error) {
	return "No error", nil
}

// Attempts returns a mapping of project names to "done" or "failed" for the given user.
func (d *Dummy) Attempts(user string) (result map[string]string, err error) {
	return
}

// Permitted returns whether the given user is allowed to access the given file.
func (d *Dummy) Permitted(file, user string) error {
	return nil
}

// ApplyAlias conditionally renders the alias of the given student, depending
// on which user asks.
func (d *Dummy) ApplyAlias(user, student string) string {
	if d._kv["alias"] != "" {
		if d.IsTeacher(user) {
			return d._kv["alias"] + " (" + student + ")"
		}

		return d._kv["alias"]
	}

	if d.IsTeacher(user) {
		return student
	}

	return "Anonymous"
}

// StatsRankings returns a mapping of projects complete to students.
func (d *Dummy) StatsRanks(user string) (byRank map[int][]string, err error) {
	return
}

// StatsCourse returns the grades for a course. A teacher can fetch all
// grades; a student receives grades with other student names redacted.
func (d *Dummy) StatsCourse(user, crs string) (grades map[string][]proj.Grade, err error) {
	return
}

// StatsGrades returns a set of grades, subject to who is asking what.
func (d *Dummy) StatsGrades(user, student, project string, brief bool) (grades []proj.Grade, err error) {
	return []proj.Grade{
		proj.Grade{
			Project:   "project1",
			Student:    d.ApplyAlias(user, "No Alias"),
			Commit:    "commit1",
			Timestamp: "timestamp1",
			Outcome:   "PASS",
			Max:        50,
			Score:      50,
		},
		proj.Grade{
			Project:   "project2",
			Student:    d.ApplyAlias(user, "No Alias"),
			Commit:    "commit1",
			Timestamp: "timestamp1",
			Outcome:   "PASS",
			Max:        100,
			Score:      50,
		},
		proj.Grade{
			Project:   "project1",
			Student:   "student1",
			Commit:    "commit1",
			Timestamp: "timestamp1",
			Outcome:   "PASS",
			Max:        25,
			Score:      10,
		},
		proj.Grade{
			Project:   "project2",
			Student:   "student1",
			Commit:    "commit2",
			Timestamp: "timestamp2",
			Outcome:   "FAIL",
			Max:        50,
			Score:      0,
		},
		proj.Grade{
			Project:   "project1",
			Student:   "student2",
			Commit:    "commit3",
			Timestamp: "timestamp3",
			Outcome:   "PASS",
			Max:        10,
			Score:      10,
		},
	}, nil
}

// Courses returns a set of courses, subject to who is asking what.
func (d *Dummy) Courses() (courses []course.Course, err error) {
	return []course.Course{
		course.Course{
			Name: "cs120",
			Events: []course.Event{
				course.Event{
					Name: "git",
					Max:   3,
				},
				course.Event{
					Name: "helloC",
					Max:   3,
				},
			},
			Enrollment: []string{"student1", "student2"},
		},
	}, nil
}

// StudentExists returns true if the given student exists.
func (d *Dummy) StudentExists(student string) bool {
	return true
}

// IsTeacher returns whether the given user is a teacher.
func (d *Dummy) IsTeacher(user string) bool {
	return true
}

// IsForked returns whether the given user has forked project.
func (d *Dummy) IsForked(user, project string) (bool, error) {
	return true, nil
}
