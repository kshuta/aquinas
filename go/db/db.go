package db

import (
	"aquinas/course"
	"errors"
	"aquinas/proj"
)

// ErrAliasExists indicates a requested alias already exists.
var ErrAliasExists error = errors.New("alias exists")

// DB is a database of account, access, and submission information.
type DB interface {
	AddStudent(user, password string) (jobUUID string, err error)
	RemoveStudent(user string) (jobUUID string, err error)
	Authenticate(user, password string) (err error)
	SetPassword(user, password string) (err error)
	Alias(user string) (alias string, err error)
	SetAlias(user, alias string) (err error)
	SSHAuthKeys(user string) (key string, err error)
	SetSSHAuthKeys(user, key string) (jobUUID string, err error)
	KV(user, k string) (v string, err error)
	SetKV(user string, k string, v string) (err error)
	Last(root, user, file string) (last string, err error)
	Attempts(user string) (result map[string]string, err error)
	Permitted(file, user string) error
	StatsRanks(user string) (byRank map[int][]string, err error)
	StatsCourse(user, crs string) (grades map[string][]proj.Grade, err error)
	StatsGrades(user, student, project string, brief bool) (grades []proj.Grade, err error)
	Courses() (courses []course.Course, err error)
	StudentExists(user string) bool
	IsTeacher(user string) bool
	IsForked(user, project string) (bool, error)
}
