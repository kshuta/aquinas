package db

import (
	"fmt"
	"strings"
	"testing"
)

func TestStatsGradesStudentOnlySelf(t *testing.T) {
	d := make(Filesystem)

	grades, err := d.StatsGrades("student1@example.com", "", "", false)
	if err != nil {
		t.Error(err.Error())
	}

	for _, r := range(grades) {
		if r.Student != "alias1 (student1@example.com)" {
			t.Error(fmt.Sprintf("data for %s in results for student1@example.com", r.Student))
		}
	}
}

func TestStatsGradesTeacherAll(t *testing.T) {
	var s1, s2 bool

	d := make(Filesystem)

	grades, err := d.StatsGrades("teacher@example.com", "", "", false)
	if err != nil {
		t.Error(err.Error())
	}

	for _, r := range(grades) {
		if r.Student == "alias1 (student1@example.com)" {
			s1 = true
		}

		if r.Student == "alias2 (student2@example.com)" {
			s2 = true
		}
	}

	if !s1 {
		t.Error("data for student1@example.com not found in results for teacher@example.com")
	}

	if !s2 {
		t.Error("data for student2@example.com not found in results for teacher@example.com")
	}
}

func TestStatsGradesStudentBrief(t *testing.T) {
	d := make(Filesystem)
	m := make(map[string]bool)

	grades, err := d.StatsGrades("student1@example.com", "", "", true)
	if err != nil {
		t.Error(err.Error())
	}

	for _, r := range(grades) {
		if _, ok := m[r.Project]; ok {
			t.Error(fmt.Sprintf("duplicate appearance of project %s in brief report", r.Project))
		}

		m[r.Project] = true
	}
}

func TestStatsGradesTeacherBrief(t *testing.T) {
	d := make(Filesystem)
	m := make(map[string]bool)

	grades, err := d.StatsGrades("teacher@example.com", "", "", true)
	if err != nil {
		t.Error(err.Error())
	}

	for _, r := range(grades) {
		key := r.Project + "/" + r.Student

		if _, ok := m[key]; ok {
			t.Error(fmt.Sprintf("duplicate appearance of project %s in brief report", r.Project))
		}

		m[key] = true
	}
}

func TestStatsRanksStudentAliases(t *testing.T) {
	d := make(Filesystem)
	found := false

	ranks, err := d.StatsRanks("student1@example.com")
	if err != nil {
		t.Error(err.Error())
	}

	for _, r1 := range(ranks) {
		for _, r2 := range(r1) {
			if strings.Contains(r2, "student2@example.com") {
				t.Error(fmt.Sprintf("email student2@example.com in results for student1@example.com"))
			}

			if r2 == "alias1 (student1@example.com)" {
				found = true
			}
		}
	}

	if !found {
		t.Error(fmt.Sprintf("student1@example.com not included along with alias"))
	}
}

func TestStatsRanksTeacherAliases(t *testing.T) {
	d := make(Filesystem)
	var s1, s2 bool

	ranks, err := d.StatsRanks("teacher@example.com")
	if err != nil {
		t.Error(err.Error())
	}

	for _, r1 := range(ranks) {
		for _, r2 := range(r1) {
			if r2 == "alias1 (student1@example.com)" {
				s1 = true
			}

			if r2 == "alias2 (student2@example.com)" {
				s2 = true
			}
		}
	}

	if !s1 {
		t.Error(fmt.Sprintf("student1@example.com not included along with alias"))
	}

	if !s2 {
		t.Error(fmt.Sprintf("student2@example.com not included along with alias"))
	}
}

func TestStatsCourseStudentRedacted(t *testing.T) {
	d := make(Filesystem)

	grades, err := d.StatsCourse("student1@example.com", "course1")
	if err != nil {
		t.Error(err.Error())
	}

	for k, _ := range(grades) {
		if strings.Contains(k, "alias2") {
			t.Error("alias2 not redacted")
		}

		if strings.Contains(k, "student2@example.com") {
			t.Error("student2@example.com not redacted")
		}
	}
}

func TestStatsCourseTeacherNotRedacted(t *testing.T) {
	d := make(Filesystem)

	grades, err := d.StatsCourse("teacher@example.com", "course1")
	if err != nil {
		t.Error(err.Error())
	}

	if _, ok := grades["student1@example.com"]; !ok {
		t.Error("data for student1@example.com not found in results for teacher@example.com")
	}

	if _, ok := grades["student2@example.com"]; !ok {
		t.Error("data for student2@example.com not found in results for teacher@example.com")
	}
}

func TestStatsCourseScores(t *testing.T) {
	d := make(Filesystem)
	var p1, p2, p3, p4 bool

	grades, err := d.StatsCourse("teacher@example.com", "course1")
	if err != nil {
		t.Error(err.Error())
	}

	for _, r := range(grades["student1@example.com"]) {
		if r.Project == "project1" {
			p1 = true
			if r.Score != 90 {
				t.Error(fmt.Sprintf("student1 project1 score is %f, not 90", r.Score))
			}
		}

		if r.Project == "project2" {
			p2 = true
			if r.Score != 60 {
				t.Error(fmt.Sprintf("student1 project1 score is %f, not 60", r.Score))
			}
		}
	}

	for _, r := range(grades["student2@example.com"]) {
		if r.Project == "project1" {
			p3 = true
			if r.Score != 75 {
				t.Error(fmt.Sprintf("student1 project1 score is %f, not 75", r.Score))
			}
		}

		/* This project has not record, but does have a hand-assigned score. */
		if r.Project == "project2" {
			p4 = true
			if r.Score != 50 {
				t.Error(fmt.Sprintf("student2 project1 score is %f, not 50", r.Score))
			}
		}
	}

	if !p1 {
		t.Error("data for p1 not found in results for teacher@example.com")
	}

	if !p2 {
		t.Error("data for p2 not found in results for teacher@example.com")
	}

	if !p3 {
		t.Error("data for p3 not found in results for teacher@example.com")
	}

	if !p4 {
		t.Error("data for p4 (hand-graded only) not found in results for teacher@example.com")
	}
}
