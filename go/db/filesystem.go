package db

import (
	"bufio"
	"aquinas/common"
	"aquinas/conf"
	"aquinas/course"
	crand "crypto/rand"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path"
	"path/filepath"
	"aquinas/proj"
	"aquinas/run"
	"strings"
	"time"
)

type file struct {
	contents  string
	timestamp time.Time
}

type record struct {
	sshAuthKeys string
	anonID      int
	last        file
	records     file
	kv          map[string]*file
}

// Filesystem is a filesystem-based database of account, access, and submission
// information.
type Filesystem map[string]*record

var nextID int

func init() {
	var b [8]byte

	_, err := crand.Read(b[:])
	if err != nil {
		panic("failed to seed math/rand package with secure random number generator")
	}

	rand.Seed(int64(binary.LittleEndian.Uint64(b[:])))
}

func newRecord() *record {
	r := new(record)
	r.kv = make(map[string]*file)

	return r
}

// AddStudent creates the given student and assigned him the given password.
func (d Filesystem) AddStudent(student, password string) (jobUUID string, err error) {
	dir  := path.Join(conf.ConfRoot(), "httpd/accounts", student)
	hash := common.HashCalc(password)

	if _, err := os.Stat(dir); err == nil {
		return "", fmt.Errorf("%s already exists", student)
	}

	if err := os.Mkdir(dir, 0755); err != nil {
		return "", fmt.Errorf("could not create %s: %s", dir, err)
	}

	pwPath := path.Join(dir, "password")
	if err := ioutil.WriteFile(pwPath, []byte(hash + "\n"), 0644); err != nil {
		return "", fmt.Errorf( "could not write to %s: %s", pwPath, err)
	}

	gitHost := conf.HostGit() + "." + conf.Domain()
	stdin := strings.NewReader("add " + student + "\n")
	stdout, _, err := run.SSH(stdin, nil, gitHost)
	if err != nil {
		return "", fmt.Errorf("failed to add student on %s: %s", gitHost, err)
	}

	jobUUID = strings.TrimSuffix(string(stdout), "\n")

	return jobUUID, err
}

// RemoveStudent removes the given student.
func (d Filesystem) RemoveStudent(student string) (jobUUID string, err error) {
	dir := path.Join(conf.ConfRoot(), "httpd/accounts", student)

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		return "", fmt.Errorf("%s does not exist", student)
	} else if err != nil {
		return "", fmt.Errorf("error finding record for %s: %s", student, err)
	}

	if err := os.RemoveAll(dir); err != nil {
		return "", fmt.Errorf("could not remove %s: %s", dir, err)
	}

	dir = path.Join(conf.DataRoot(), "www", student)
	if err := os.RemoveAll(dir); err != nil {
		return "", fmt.Errorf("could not remove %s: %s", dir, err)
	}

	gitHost := conf.HostGit() + "." + conf.Domain()
	stdin := strings.NewReader("remove " + student + "\n")
	stdout, _, err := run.SSH(stdin, nil, gitHost)
	if err != nil {
		return "", fmt.Errorf("failed to remove student on %s: %s", gitHost, err)
	}

	jobUUID = strings.TrimSuffix(string(stdout), "\n")

	delete(d, student)

	return
}

func (d Filesystem) passwordHash(user string) (hash string, err error) {
	passwordPath := path.Join(conf.ConfRoot(), "httpd/accounts", user, "password")

	password, err := ioutil.ReadFile(passwordPath)
	if err != nil {
		return
	}

	hash = strings.TrimSuffix(string(password), "\n")

	return
}

// Authenticate checks to see if the given password is correct for the given
// user.
func (d Filesystem) Authenticate(user, password string) (err error) {
	hashedPassword, err := d.passwordHash(user)
	if err != nil {
		err = errors.New("received bad login credentials: " +
		                 "could not read password for " + user)
		return
	}

	if !common.HashSame(hashedPassword, common.HashCalc(password)) {
		err = errors.New("received bad login credentials: bad " +
		                 "password for " + user)
		return
	}

	return
}

// SetPassword sets the given user's password to the given value.
func (d Filesystem) SetPassword(user, password string) (err error) {
	userPath := path.Join(conf.ConfRoot(), "httpd/accounts", user)
	if _, err = os.Stat(userPath); os.IsNotExist(err) {
		err = os.Mkdir(userPath, 0750)
	}
	if err != nil {
		return
	}

	hash := common.HashCalc(password)

	passwordPath := path.Join(userPath, "password")
	err = ioutil.WriteFile(passwordPath, []byte(hash + "\n"), 0644)
	return
}

// Alias gets the alias for the given user.
func (d Filesystem) Alias(user string) (alias string, err error) {
	return d.KV(user, "alias")
}

// SetAlias sets the given user's password to the given value.
func (d Filesystem) SetAlias(user, alias string) (err error) {
	/* Check to see if alias already in use. */
	dir := path.Join(conf.ConfRoot(), "httpd/accounts")
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return
	}

	for _, info := range files {
		p := path.Join(dir, info.Name(), "alias")

		if _, err = os.Stat(p); os.IsNotExist(err) {
			continue
		}

		alias2, err := ioutil.ReadFile(p)
		if err != nil {
			return err
		}

		if alias + "\n" == string(alias2) {
			return ErrAliasExists
		}
	}

	return d.SetKV(user, "alias", alias)
}

// KV gets the value corresponding with the given key for the given user.
func (d Filesystem) KV(user, k string) (v string, err error) {
	p := path.Join(conf.ConfRoot(), "httpd/accounts", user, k)
	info, err := os.Stat(p)
	if err == nil {
		r, found := d[user]
		if !found {
			d[user] = newRecord()
		} else if _, ok := r.kv[k]; ok && r.kv[k].timestamp.Equal(info.ModTime()) {
			return r.kv[k].contents, nil
		}
	} else if !os.IsNotExist(err) {
		return "", fmt.Errorf(k + " stat failed: %v", err)
	} else {
		return "", nil
	}

	_v, err := ioutil.ReadFile(p)
	if err != nil {
		return "", fmt.Errorf(k + " read failed: %v", err)
	}

	v = strings.TrimSuffix(string(_v), "\n")
	f := new(file)
	f.contents  = v
	f.timestamp = info.ModTime()
	d[user].kv[k] = f

	return
}

// SetKV sets the given user's key to the given value.
func (d Filesystem) SetKV(user, k, v string) (err error) {
	p := path.Join(conf.ConfRoot(), "httpd/accounts", user, k)
	return ioutil.WriteFile(p, []byte(v + "\n"), 0644)
}

// SSHAuthKeys gets the SSH keys for the given user.
func (d Filesystem) SSHAuthKeys(user string) (key string, err error) {
	if r, found := d[user]; !found {
		d[user] = newRecord()
	} else if r.sshAuthKeys != "" {
		return r.sshAuthKeys, nil
	}

	stdin := strings.NewReader("ssh " + user + "\n")
	output, _, err := run.SSH(stdin, nil, conf.HostGit() + "." + conf.Domain())
	if err != nil {
		return "", fmt.Errorf("could not retrieve SSH key from Git host: %v", err)
	}

	key = strings.TrimSuffix(string(output), "\n")
	d[user].sshAuthKeys = key

        return
}

// SetSSHAuthKeys sets the SSH keys for the given user to the given value.
func (d Filesystem) SetSSHAuthKeys(user, key string) (jobUUID string, err error) {
	/* Encode key to remove spaces. */
	encodedKey := base64.StdEncoding.EncodeToString([]byte(key))

	stdin := strings.NewReader("key " + user + " " + encodedKey + "\n")
	u, _, err := run.SSH(stdin, nil, conf.HostGit() + "." + conf.Domain())
	if err != nil {
		return
	}

	jobUUID = strings.TrimSuffix(string(u), "\n")

	return
}

// Last returns the last submission record for the given user and file (project).
func (d Filesystem) Last(root, user, filename string) (last string, err error) {
	project := strings.TrimSuffix(filename, path.Ext(filename)) + "-last"

	p := path.Join(root, user, project)
	info, err := os.Stat(p)
	if err == nil {
		r, found := d[user]
		if !found {
			d[user] = newRecord()
			d[user].kv = make(map[string]*file)
		} else if r.last.timestamp.Equal(info.ModTime()) {
			return r.last.contents, nil
		}
	} else if !os.IsNotExist(err) {
		return "", fmt.Errorf("last stat failed: %v", err)
	} else {
		return "", nil
	}

	_last, err := ioutil.ReadFile(p)
	if err != nil {
		return "", fmt.Errorf("last read failed: %v", err)
	}

	last = strings.TrimSuffix(string(_last), "\n")
	d[user].last.contents  = last
	d[user].last.timestamp = info.ModTime()

	return
}

// Attempts returns a mapping of project names to "done" or "failed" for the given user.
func (d Filesystem) Attempts(user string) (result map[string]string, err error) {
	dir    := path.Join(conf.DataRoot(), "www", user)
	suffix := "-records"
	result  = make(map[string]string)

	files, err := ioutil.ReadDir(dir)
	if os.IsNotExist(err) {
		return result, nil
	}
	if err != nil {
		return result, fmt.Errorf("attempts readdir failed: %v", err)
	}

	for _, info := range files {
		var f *os.File
		var ok bool

		if !strings.HasSuffix(info.Name(), suffix) {
			continue
		}

		proj := strings.TrimSuffix(info.Name(), suffix)

		f, err = os.Open(path.Join(dir, info.Name()))
		if os.IsNotExist(err) {
			err = nil
			continue
		}
		if err != nil {
			return
		}
		defer f.Close()

		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			if strings.Contains(scanner.Text(), "PASS") {
				ok = true
				break
			}
		}

		if ok {
			result[proj] = "done"
		} else {
			result[proj] = "failed"
		}
	}

	return
}

// Permitted returns whether the given user is allowed to access the given file.
func (d Filesystem) Permitted(file, user string) error {
	if file == "" {
		/* Root is always permitted. */
		return nil
	}

	/*
	 * Normalize upon path without api/.  For example, we might
	 * need to restrict both project/x and api/project/x using
	 * /etc/httpd/restrictions/project/foo.
	 */
	file = strings.TrimPrefix(file, "api/")

	base := strings.TrimSuffix(file, filepath.Ext(file))
	restrict := path.Join(conf.ConfRoot(), "httpd/restrictions", base)

	files, err := ioutil.ReadDir(restrict)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}

		return err
	}

	if user != "" {
		for _, file := range files {
			if file.Name() == user {
				return nil
			}
		}
	}

	return errors.New("project is restricted")
}

// ApplyAlias conditionally renders the alias of the given student, depending
// on which user asks.
func (d Filesystem) ApplyAlias(user, student string) string {
	alias, err := d.Alias(student)
	if err != nil || alias == "" {
		if d.IsTeacher(user) || user == student {
			return student
		}

		if _, ok := d[student]; !ok {
			nextID++
			d[student] = newRecord()
			d[student].anonID = nextID
		} else if d[student].anonID == 0 {
			nextID++
			d[student].anonID = nextID
		}

		return fmt.Sprintf("Anonymous #%d", d[student].anonID)
	}

	if d.IsTeacher(user) || user == student {
		return alias + " (" + student + ")"
	}

	return alias
}

// StatsRankings returns a mapping of projects complete to students.
func (d Filesystem) StatsRanks(user string) (byRank map[int][]string, err error) {
	var students []os.FileInfo

	byRank = make(map[int][]string)
	byStudent := make(map[string]int)

	// Prepare to gather all grades (check for @ later).
	students, err = ioutil.ReadDir(path.Join(conf.DataRoot(), "www"))
	if err != nil {
		return nil, err
	}

	for _, info1 := range students {
		student2 := info1.Name()

		if !strings.Contains(student2, "@") {
			/*
			 * Test account, and not explicitly requested or not
			 * user directory.
			 */
			continue
		}

		records, err := ioutil.ReadDir(path.Join(conf.DataRoot(), "www", student2))
		if err != nil {
			return nil, err
		}

		for _, info2 := range records {
			var grade2 []proj.Grade

			record := info2.Name()

			if !strings.HasSuffix(record, "-records") {
				// Not a records file.
				continue
			}

			f, err := os.Open(path.Join(conf.DataRoot(), "www", student2, record))
			if err != nil {
				if !os.IsNotExist(err) {
					return nil, err
				}

				// Does not exist; skip.
				continue
			}

			dec := json.NewDecoder(f)

			err = dec.Decode(&grade2)
			f.Close()
			if err != nil {
				// Cannot decode; skip.
				continue
			}

			/*
			 * Shrink list to first PASS or last FAIL.
			 */
			grade3 := proj.Grade{}

			for _, g := range grade2 {
				grade3 = g

				if g.Outcome == "PASS" {
					break
				}
			}

			if grade3.Outcome == "PASS" {
				alias := d.ApplyAlias(user, student2)

				if _, ok := byStudent[alias]; ok {
					byStudent[alias] += 1
				} else {
					byStudent[alias] = 1
				}
			}

		}
	}

	for k, v := range(byStudent) {
		byRank[v] = append(byRank[v], k)
	}

	return
}

// StatsCourse returns the grades for a course. A teacher can fetch all
// grades; a student receives grades with other student names redacted.
func (d Filesystem) StatsCourse(user, crs string) (grades map[string][]proj.Grade, err error) {
	var students []os.FileInfo
	var courseData course.Course

	courses, err := d.Courses()
	if err != nil {
		return nil, err
	}

	// Prepare to gather all grades (check for @ later).
	students, err = ioutil.ReadDir(path.Join(conf.DataRoot(), "www"))
	if err != nil {
		return nil, err
	}

	// Find course.
	for _, c := range(courses) {
		if c.Name == crs {
			courseData = c
		}
	}

	grades = make(map[string][]proj.Grade)

	for _, info1 := range students {
		student2 := info1.Name()

		if user != student2 && !strings.Contains(student2, "@") {
			/*
			 * Test account, and not explicitly requested or not
			 * user directory.
			 */
			continue
		}

		var found bool

		// Check if student in course.
		for _, n := range(courseData.Enrollment) {
			if n == student2 {
				found = true
			}
		}

		if !found {
			// This student not in course; skip.
			continue
		}

		for _, event := range courseData.Events {
			var grade2 []proj.Grade

			// Try to read record for this project.
			filename := event.Name + "-records"
			f, err := os.Open(path.Join(conf.DataRoot(), "www", student2, filename))
			if err != nil {
				if !os.IsNotExist(err) {
					return nil, err
				}
			} else {
				dec := json.NewDecoder(f)

				err = dec.Decode(&grade2)
				f.Close()
				if err != nil {
					return nil, err
				}
			}

			/*
			 * If a student submits nothing, then he might
			 * lack an autograded record despite having
			 * a hand-graded score. Reconcile here.
			 *
			 * The student must still have a *-record file
			 * containing "[]", which is created by
			 * aquinas-manual-grade under these conditions.
			 */
			if len(grade2) == 0 {
				grade2 = []proj.Grade{proj.Grade{Project: event.Name, Student: student2, Max: event.Max}}
			}

			score := float64(-1)

			// Try to read the manual score for this project.
			filename = fmt.Sprintf("%s-%s-score", event.Name, crs)
			f, err = os.Open(path.Join(conf.DataRoot(), "www", student2, filename))
			if err != nil {
				if !os.IsNotExist(err) {
					return nil, err
				}

				score = -1
			} else {
				dec := json.NewDecoder(f)

				err = dec.Decode(&score)
				f.Close()
				if err != nil {
					// Fix possible partial decode.
					score = -1
				}
			}

			/*
			 * Shrink list to first PASS or last FAIL,
			 * and set score.
			 */
			grade3 := grade2

			for _, g := range grade2 {
				g.Score = score
				g.Max = event.Max

				grade3 = []proj.Grade{g}

				if g.Outcome == "PASS" {
					break
				}
			}

			grade2 = grade3

			if _, ok := grades[student2]; !ok {
				grades[student2] = grade2
			} else {
				grades[student2] = append(grades[student2], grade2...)
			}
		}
	}

	redacted := make(map[string]string)

	// This relies on the map to jumble things to avoid leaking information
	// about redacted students due to order.
	grades2 := make(map[string][]proj.Grade)
	redactedID := 1
	for k, v := range(grades) {
		for _, g := range(v) {
			if !d.IsTeacher(user) && k != user {
				if _, ok := redacted[k]; !ok {
					redacted[k] = fmt.Sprintf("[Redacted %03d]", redactedID)
					redactedID += 1
				}

				g.Student = redacted[k]

				if _, ok := grades2[redacted[k]]; !ok {
					grades2[redacted[k]] = []proj.Grade{g}
				} else {
					grades2[redacted[k]] = append(grades2[redacted[k]], g)
				}
			} else {
				if _, ok := grades2[g.Student]; !ok {
					grades2[g.Student] = []proj.Grade{g}
				} else {
					grades2[g.Student] = append(grades2[g.Student], g)
				}
			}
		}
	}

	return grades2, nil
}

// StatsGrades returns all grades, the grades for a student, or the grades for
// a projects. Students cannot retrieve other student grades. Teachers can
// retrieve all grades.
func (d Filesystem) StatsGrades(user, student, project string, brief bool) (grades []proj.Grade, err error) {
	var students []os.FileInfo

	// Set student to current user if not specified and current user not teacher.
	if !d.IsTeacher(user) && student == "" {
		student = user
	}

	if student != "" {
		// Prepare to gather grades for one student.
		s, err := os.Stat(path.Join(conf.DataRoot(), "www", student))
		if err != nil {
			if os.IsNotExist(err) {
				/* Student dir. not yet created; no grades */
				return []proj.Grade{}, nil
			}

			return []proj.Grade{}, err
		}
		students = []os.FileInfo{s}
	} else {
		// Prepare to gather all grades (check for @ later).
		students, err = ioutil.ReadDir(path.Join(conf.DataRoot(), "www"))
		if err != nil {
			return []proj.Grade{}, err
		}
	}

	for _, info1 := range students {
		student2 := info1.Name()

		if student != student2 && !strings.Contains(student2, "@") {
			/*
			 * Test account, and not explicitly requested or not
			 * user directory.
			 */
			continue
		}

		records, err := ioutil.ReadDir(path.Join(conf.DataRoot(), "www", student2))
		if err != nil {
			return []proj.Grade{}, err
		}

		for _, info2 := range records {
			var grade2 []proj.Grade

			record := info2.Name()

			if !strings.HasSuffix(record, "-records") {
				// Not a records file.
				continue
			}

			thisProj := strings.TrimSuffix(record, "-records")
			if project != "" && project != thisProj {
				// Requested project, but not this one.
				continue
			}

			f, err := os.Open(path.Join(conf.DataRoot(), "www", student2, record))
			if err != nil {
				if !os.IsNotExist(err) {
					return []proj.Grade{}, err
				}

				// Does not exist; skip.
				continue
			}

			dec := json.NewDecoder(f)

			err = dec.Decode(&grade2)
			f.Close()
			if err != nil {
				// Cannot decode; skip.
				continue
			}

			if brief {
				/*
				 * Shrink list to first PASS or last FAIL.
				 */
				grade3 := []proj.Grade{}

				for _, g := range grade2 {
					if g.Outcome == "PASS" {
						grade3 = []proj.Grade{g}
						break
					}

					grade3 = []proj.Grade{g}
				}

				grade2 = grade3
			}

			grades = append(grades, grade2...)
		}
	}

	for i := range grades {
		grades[i].Student = d.ApplyAlias(user, grades[i].Student)
	}

	return grades, nil
}

// Courses returns a set of courses.
func (d Filesystem) Courses() (courses []course.Course, err error) {
	f, err := os.Open(path.Join(conf.ConfRoot(), "httpd/courses/courses"))
	if err == nil {
		dec := json.NewDecoder(f)

		err = dec.Decode(&courses)
		f.Close()
	}

	return
}

// StudentExists returns true if the given student exists.
func (d Filesystem) StudentExists(student string) bool {
	_, err := os.Stat(path.Join(conf.ConfRoot(), "httpd/accounts", student))

	return err == nil
}

// IsTeacher returns whether given the user is a teacher.
func (d Filesystem) IsTeacher(user string) bool {
	_, err := os.Stat(path.Join(conf.ConfRoot(), "httpd/accounts", user, "teacher"))
	return !os.IsNotExist(err)
}

// IsForked returns whether the given user has forked project.
func (d Filesystem) IsForked(user, project string) (bool, error) {
	var repo common.GitRepository
	var err error

	repo.Provider, err = d.KV(user, "git-provider")
	if err != nil {
		return false, fmt.Errorf("could not retrieve fork status from Git host: %s", err)
	}

	repo.Path, err = d.KV(user, "git-path")
	if err != nil {
		return false, fmt.Errorf("could not retrieve fork status from Git host: %s", err)
	}

	repo.Username, err = d.KV(user, "git-username")
	if err != nil {
		return false, fmt.Errorf("could not retrieve fork status from Git host: %s", err)
	}

	repo.Token, err = d.KV(user, "git-token")
	if err != nil {
		return false, fmt.Errorf("could not retrieve fork status from Git host: %s", err)
	}

	j, err := json.Marshal(repo)
	if err != nil {
		return false, fmt.Errorf("could not retrieve fork status from Git host: %s", err)
	}

	stdin := strings.NewReader("is-forked " + user + " " + project + " " + string(j) + "\n")
	output, _, err := run.SSH(stdin, nil, conf.HostGit() + "." + conf.Domain())
	if err != nil {
		return false, fmt.Errorf("could not retrieve fork status from Git host: %s", err)
	}

	if string(output) == "true\n" {
		return true, nil
	}

	return false, nil
}
