Aquinas supports using GitLab to host student submissions.  Please ensure
the Git repositories containing your work are available only to you,
your Git provider, and Aquinas. We ask that you not share your work with
others, according to the Aquinas user agreement.

Students who prefer GitLab can tie their accounts to GitLab by following
these steps:

1. Arrange for a GitLab Access Token. Visit you account settings page,
and select "Access Tokens" on the left. Enter a name; select an expiration date;
check "api", "read_repository", and "write_repository; and click "Create
personal access token."

2. Introduce Aquinas to your GitLab account. Visit the Aquinas'
Account -> Set Git provider utility. Select "GitLab" as the Git provider,
enter the URL below, and select "Submit." Here is an example URL:
`https://username:PfncTfi4Hh6hSrenS7V8@gitlab.com/username/PROJECT`.

3. Fork a project. Select a project that requires submissions. Click on
the "fork" link to cause Aquinas to create a new repository on GitLab
using your access token.

4. Confirm your project exists on GitLab. Visit
`https://gitlab.com/username/project, where "username" is your GitLab
username and "project" is the project you forked.

5. Complete the project. Return to the project page on Aquinas, and clone
the repository by following the instructions therein. Finish your solution
and push it to GitLab.

6. Submit for grading. Return to the project page on Aquinas, and click
the "Regrade" button. You should soon see your grade reflected in the
"Records" table.
