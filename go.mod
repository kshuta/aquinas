module main

go 1.16

require (
	aquinas/common v0.0.0-00010101000000-000000000000
	aquinas/conf v0.0.0-00010101000000-000000000000
	aquinas/course v0.0.0-00010101000000-000000000000
	aquinas/db v0.0.0-00010101000000-000000000000
	aquinas/kahn v0.0.0-00010101000000-000000000000
	aquinas/limits v0.0.0-00010101000000-000000000000
	aquinas/proj v0.0.0-00010101000000-000000000000
	aquinas/queue v0.0.0-00010101000000-000000000000
	aquinas/run v0.0.0-00010101000000-000000000000
	aquinas/student v0.0.0-00010101000000-000000000000
	aquinas/wasm v0.0.0-00010101000000-000000000000
	flyn.org/git/safe.git v0.0.0-20210810235404-39861e08a7b1
	flyn.org/git/wasm.git v0.0.0-20210720143558-250dc816709b
	github.com/dop251/goja v0.0.0-20211203105952-bf6af58bbcc8 // indirect
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	golang.org/x/net v0.0.0-20210504132125-bbd867fde50d
	golang.org/x/sys v0.0.0-20210503173754-0981d6026fa6
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
	modernc.org/sqlite v1.10.4
)

replace aquinas/common => ./go/common

replace aquinas/conf => ./go/conf

replace aquinas/course => ./go/course

replace aquinas/db => ./go/db

replace aquinas/kahn => ./go/kahn

replace aquinas/limits => ./go/limits

replace aquinas/proj => ./go/proj

replace aquinas/queue => ./go/queue

replace aquinas/run => ./go/run

replace aquinas/student => ./go/student

replace aquinas/wasm => ./go/wasm
