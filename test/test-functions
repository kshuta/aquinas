. ../aquinas-functions

if [ -z "$git_json" ]; then
	git_json="{}"
fi

msg() {
	printf "%s" "${1}: "
	shift
	printf "%s" "${1}: "
	shift
	echo "${*}"
}

failmsg() {
	msg "[ ] FAIL" "$@"
	exit 1
}

passmsg() {
	msg "[+] PASS" "$@"
	exit 0
}

passmsg2() {
	msg "[+] PASS" "$@"
}

failbad() {
	exitcode="$1"
	shift
	name="$1"
	shift
	string="$*"
	if [ "$exitcode" != 0 ]; then
		failmsg "$name" "$exitcode: $string"
	fi
}

failgood() {
	exitcode="$1"
	shift
	if [ "$exitcode" = 0 ]; then
		exitcode=1
	else
		exitcode=0
	fi
	failbad $exitcode "$@"
}

installkey() {
	local student="$1"
	local norm=$(calcnorm "$student")

	ssh "root@$hostGit.$domain" mkdir -p "/mnt/xvdb/$student/.ssh/"
	scp -q ~/.ssh/id_rsa.pub "root@$hostGit.$domain:/mnt/xvdb/$student/.ssh/authorized_keys"
	ssh "root@$hostGit.$domain" chown -R "$norm:$norm" "/mnt/xvdb/$student/.ssh/"
	ssh "root@$hostGit.$domain" chmod 700 "/mnt/xvdb/$student/.ssh/"
	ssh "root@$hostGit.$domain" chmod 600 "/mnt/xvdb/$student/.ssh/authorized_keys"
}

testcommit() {
	local testname="$1"
	local proj="$2"
	local lang="$3"
	local src="$4"
	local prog="$5"
	local rest="$6"
	local pass_value="$7"
	local last_value="$8"

	local student="$testCredit"
	local norm=$(calcnorm "$testCredit")

	local dir=$(mktemp -d)
	local rand=$RANDOM

	trap "/bin/rm -rf $dir" SIGTERM SIGINT EXIT

	ssh "root@$hostGit.$domain" aquinas-fork-project "$student" "$proj" "$git_json" "$lang"

	# Either clone from $hostGit or Git provider.
	if [ "$git_json" = {} ]; then
		error=$(git clone -q "$norm@$hostGit.$domain:$dataRoot/$student/$proj$lang" "$dir" 2>&1)
		failbad $? "$testname" "clone failed: ${error:-no stderr}"
	else
		error=$(git clone -q "https://${git_username}:${git_token}@gitlab.com/${git_path}/${proj}${lang}" "$dir" 2>&1)
		failbad $? "$testname" "check failed: ${error:-no stderr}"
	fi

	pushd "$dir" >/dev/null

	if [ -n "$rest" ]; then
		cp -a $rest .
		git add ./* >/dev/null
	fi

	# Dump a random value to cause a committable change.
	if [ -z "$prog" ]; then
		# Source tree; just place the random value in rand.
		echo $rand >rand
		git add rand >/dev/null
	elif [ -z "$src" ]; then
		# Python or another shebang language; edit program directly.
		echo "$prog" | sed "s/\$rand/$rand/g" >"$proj"
		chmod +x "$proj"
		git add "$proj" >/dev/null
	else
		# Edit source.
		echo "$prog" | sed "s/\$rand/$rand/g" >"$src"
		git add "$src" >/dev/null
	fi

	git commit -q -s -m "Another revision for testing: $rand" >/dev/null

	output=$(git push -q 2>&1)
	failbad $? "$testname" "git push failed: ${output:-no stderr}"

	# Either trigger by pushing to $hostGit or manually when using
	# alternate Git provider.
	if [ "$git_json" = {} ]; then
		uuid=$(echo "$output" | cut -d " " -f 2)
		failbad $? "$testname" "gathering UUID failed: ${uuid:-no stderr}"

		error=$(waitlog 600 "root@$logServer" "$uuid" /mnt/xvdb/var/log/messages 2>&1)
		failbad $? "$testname" "waitlog failed: ${error:-no stderr}"
	else
		# Escape hell: remove expansion, since this does not go through ssh.
		UNESCAPED=$(echo "$git_json" | sed 's/\\\\/\\/g' | sed 's/\\"/"/g')
		error=$(echo check "$student" "$proj$lang" "$UNESCAPED" | ssh -T "root@$hostGit.$domain" sudo -u http httpsh)
		failbad $? "$testname" "check failed: ${error:-no stderr}"
	fi

	hash=$(git log -1 --pretty=%H)

	popd >/dev/null

	record=$dataRoot/teacher/workdir/records/$proj/$student$lang.record
	error=$(ssh "root@$hostGit.$domain" grep -q "$hash.*\"$pass_value\"" "$record" 2>&1)
	failbad $? "$testname" "pass value wrong: ${error:-no stderr}"

	last=$dataRoot/www/$student/$proj$lang-last
	error=$(ssh "root@$hostWWW.$domain" grep -q "\"$last_value\"" "$last" 2>&1)
	failbad $? "$testname" "last value failed: $last_value not in $last: ${error:-no stderr}"
}

removestudent() {
	local student=$1

	uuid=$(ssh root@$hostWWW.$domain sudo -u http aquinas-remove-student $student 2>&1)
	failbad $? "$0" "aquinas-remove-student failed: ${uuid:-no stderr}"

	error=$(waitlog 600 root@$logServer "$uuid" /mnt/xvdb/var/log/messages 2>&1)
	failbad $? "$0" "waitlog (remove) failed: ${error:-no stderr}"
}

removestudentquiet() {
	local student=$1

	# Check if student exists, and remove him if he does.
	if ssh root@$hostWWW.$domain [ -d "/etc/httpd/accounts/$student" ]; then
		uuid=$(ssh root@$hostWWW.$domain sudo -u http aquinas-remove-student $student 2>&1)
		waitlog 600 root@$logServer "$uuid" /mnt/xvdb/var/log/messages 2>&1
	fi
}

studentcreated() {
	local student="$1"
	local norm=$(calcnorm "$student")

	# Account exists on $hostWWW.
	error=$(ssh -T "root@$hostWWW.$domain" [ -f "/etc/httpd/accounts/$student/password" ] 2>&1)
	failbad $? "$0" "/etc/httpd/accounts/$student/password missing"

	# Account exists on $hostGit.
	error=$(ssh -T "root@$hostGit.$domain" grep -q "^$norm:" /etc/passwd 2>&1)
	failbad $? "$0" "$norm not in $hostGit's /etc/passwd"

	# Account exists on $hostUser.
	error=$(ssh -T "root@$hostUser.$domain" grep -q "^$norm:" /etc/passwd 2>&1)
	failbad $? "$0" "$norm not in $hostUser's /etc/passwd"

	# Home directory exists on $hostGit.
	error=$(ssh -T "root@$hostGit.$domain" [ -d "$dataRoot/$student" ] 2>&1)
	failbad $? "$0" "no $dataRoot/$student on $hostGit"

	# No project repository if no check.
	error=$(ssh -T "root@$hostGit.$domain" [ ! -d "$dataRoot/$student/unix" ] 2>&1)
	failbad $? "$0" "$dataRoot/$student/unix already exists on $hostGit"

	# Home directory exists on $hostUser.
	error=$(ssh -T "root@$hostUser.$domain" [ -d "/home/$student" ] 2>&1)
	failbad $? "$0" "no /home/$student on $hostUser"
}

studentremoved() {
	local student="$1"
	local norm=$(calcnorm "$student")

	# Account removed from $hostWWW.
	error=$(ssh -T "root@$hostWWW.$domain" [ -d "/etc/httpd/accounts/$student/" ] 2>&1)
	failgood $? "$0" "$student still exists in /etc/httpd/accounts"

	# Records removed from $hostWWW.
	error=$(ssh -T "root@$hostWWW.$domain" [ -d "$dataRoot/www/$student/" ] 2>&1)
	failgood $? "$0" "$student records remain in $dataRoot/www/$student"

	# Account removed from $hostGit.
	error=$(ssh -T "root@$hostGit.$domain" grep -q "^$norm:" /etc/passwd 2>&1)
	failgood $? "$0" "$norm still in $hostGit's /etc/passwd"

	# Home directory removed from $hostGit.
	error=$(ssh -T "root@$hostGit.$domain" [ -d "$dataRoot/$student" ] 2>&1)
	failgood $? "$0" "$dataRoot/$student remains on $hostGit"

	# Teacher workdir removed from $hostGit.
	error=$(ssh -T "root@$hostGit.$domain" [ -d "$dataRoot/teacher/workdir/students/$student" ] 2>&1)
	failgood $? "$0" "$dataRoot/teacher/workdir/students/$student remains on $hostGit"

	# Records removed from $hostGit.
	count=$(ssh -T "root@$hostGit.$domain" find "$dataRoot/teacher/workdir/records" -name "${student}*" | wc -l 2>&1)
	if [ "x$count" != x0 ]; then
		failmsg "$0" "record remains on $hostGit at $dataRoot/teacher/workdir/records"
	fi

	# Account removed from $hostUser.
	error=$(ssh -T "root@$hostUser.$domain" grep -q "^$norm:" /etc/passwd 2>&1)
	failgood $? "$0" "$norm still in $hostUser's /etc/passwd"

	# Home directory removed from $hostUser.
	error=$(ssh -T "root@$hostUser.$domain" [ -d "/home/$student" ] 2>&1)
	failgood $? "$0" "/home/$student remains on $hostUser"
}

mkwebdir() {
	local student=$1
	ssh root@$hostWWW.$domain mkdir -p $dataRoot/www/test/
	ssh root@$hostWWW.$domain chmod 2774 $dataRoot/www/test/
	ssh root@$hostWWW.$domain chown teacher:www-data $dataRoot/www/test/
}

injectresult() {
	local student=$1
	local project=$2
	local outcome=$3

	ssh root@$hostWWW.$domain touch "$dataRoot/www/$student/$project-records"
	ssh root@$hostWWW.$domain chown teacher:www-data "$dataRoot/www/$student/$project-records"
	ssh root@$hostWWW.$domain chmod 640 "$dataRoot/www/test/$project-records"
	ssh root@$hostWWW.$domain echo "[{\\\"project\\\": \\\"$project\\\", \\\"student\\\": \\\"$student\\\", \\\"commit\\\": \\\"ffffffffffffffffffffffffffffffffffffffff\\\", \\\"timestamp\\\": \\\"1970/01/01 00:00:00\\\", \\\"outcome\\\": \\\"$outcome\\\"}]" \>"$dataRoot/www/test/$project-records"
}

urlencode() {
	local length="${#1}"
	for (( i = 0; i < length; i++ )); do
		local c="${1:i:1}"
		case $c in
		[a-zA-Z0-9.~_-]) printf "%s" "$c" ;;
		*) printf '%s' "$c" | xxd -p -c1 |
		   while read c; do printf '%%%s' "$c"; done ;;
		esac
	done
}

waitlog() {
	local timeout="$1"
	local userserver="$2"
	local regexp="$3"
	local log="$4"

	timeout "$timeout" ssh "$userserver" tail -n 1000 -f "\"$log\"" \| grep -ql "\"$regexp\""
}
